package com.codeapp.result

import org.datacontract.schemas._2004._07.mystifly_onepoint
import com.codeapp.entities.Extension._
import com.codeapp.entities.Target
import com.codeapp.entities.Target.Target

import scala.collection.JavaConverters._

case class FareRules11Result(conversationId: Option[String]
                             , target: Target
                             , baggageInfos: Seq[BaggageInfo]
                             , fareRules: Option[Seq[FareRule]])

object FareRules11Result {
  def apply(response: mystifly_onepoint.AirRulesRS): FareRules11Result =
    validateResponse(response.isSuccess, response.getErrors) {
      FareRules11Result(
        Option(response.getConversationId).map(_.getValue)
        , Target.withName(response.getTarget.value())
        , response.getBaggageInfos.getValue.getBaggageInfo.asScala.map(BaggageInfo.apply)
        , Option(response.getFareRules).flatMap(rules => Option(rules.getValue)).map(_.getFareRule.asScala.map(FareRule.apply))
      )
    }
}

case class FareRule(airline: String
                    , cityPair: String
                    , fareBasis: Option[String]
                    , ruleDetails: Seq[RuleDetail])

object FareRule {
  def apply(response: mystifly_onepoint.FareRule): FareRule =
    FareRule(
      response.getAirline.getValue
      , response.getCityPair.getValue
      , Option(response.getFareBasis).map(_.getValue)
      , response.getRuleDetails.getValue.getRuleDetail.asScala.map(RuleDetail.apply)
    )
}

case class RuleDetail(category: String, rules: String)

object RuleDetail {
  def apply(response: mystifly_onepoint.RuleDetail): RuleDetail =
    RuleDetail(
      response.getCategory.getValue
      , response.getRules.getValue
    )
}

case class BaggageInfo(arrival: String, departure: String, flightNo: String, baggage: String)

object BaggageInfo {
  def apply(response: mystifly_onepoint.BaggageInfo): BaggageInfo =
    BaggageInfo(
      response.getArrival.getValue,
      response.getDeparture.getValue,
      response.getFlightNo.getValue,
      response.getBaggage.getValue
    )
}