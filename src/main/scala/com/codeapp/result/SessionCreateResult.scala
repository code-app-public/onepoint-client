package com.codeapp.result

import com.codeapp.entities.Target
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.mystifly
import com.codeapp.entities.Extension._

case class SessionCreateResult(conversationId: Option[String]
                               , target: Target
                               , sessionId: String)

object SessionCreateResult {
  def apply(response: mystifly.SessionCreateRS): SessionCreateResult =
    validateResponse(response.isSessionStatus, response.getErrors) {
      SessionCreateResult(
        Option(response.getConversationId).map(_.getValue)
        , Target.withName(response.getTarget.value())
        , response.getSessionId.getValue
      )
    }
}
