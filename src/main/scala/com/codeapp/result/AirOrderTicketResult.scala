package com.codeapp.result

import com.codeapp.entities.Extension._
import com.codeapp.entities.Target
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.mystifly.AirOrderTicketRS


case class AirOrderTicketResult(conversationId: Option[String]
                                , message: String
                                , target: Target
                                , uniqueId: String)

object AirOrderTicketResult {
  def apply(response: AirOrderTicketRS): AirOrderTicketResult =
    validateResponse(response.isSuccess, response.getErrors)  {
      AirOrderTicketResult(
        Option(response.getConversationId).map(_.getValue)
        , response.getMessage.getValue
        , Target.withName(response.getTarget.value())
        , response.getUniqueID.getValue
      )
    }
}

