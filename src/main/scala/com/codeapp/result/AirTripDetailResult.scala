package com.codeapp.result

import org.datacontract.schemas._2004._07.mystifly.AirTripDetailsRS
import com.codeapp.entities._
import com.codeapp.entities.Extension._
import com.codeapp.entities.Target
import com.codeapp.entities.Target.Target

import scala.collection.JavaConverters._

case class AirTripDetailResult(conversationId: Option[String] = None
                               , specialMessage: Option[Seq[String]] = None
                               , target: Target
                               , travelItinerary: Option[TravelItinerary])

object AirTripDetailResult {
  def apply(response: AirTripDetailsRS): AirTripDetailResult =
    validateResponse(response.isSuccess, response.getErrors) {
      AirTripDetailResult(
        Option(response.getConversationId).map(_.getValue)
        , Option(response.getSpecialMessage).map(_.getValue.getString.asScala)
        , Target.withName(response.getTarget.value())
        , Option(response.getTravelItinerary.getValue).map(TravelItinerary(_))
      )
    }
}
