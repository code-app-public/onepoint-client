package com.codeapp.result

import com.codeapp.entities._
import com.codeapp.entities.Extension._
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

case class AirBookResult(clientUTCOffset: Double
                         , conversationId: Option[String]
                         , isPriceChange: Option[String]
                         , isScheduleChange: Option[String]
                         , status: String
                         , target: Target
                         , tktTimeLimit: Option[DateTime]
                         , uniqueId: String)

object AirBookResult {
  def apply(response: mystifly.AirBookRS): AirBookResult =
    validateResponse(response.isSuccess, response.getErrors) {
      AirBookResult(
        response.getClientUTCOffset
        , Option(response.getConversationId).map(_.getValue)
        , Option(response.getIsPriceChange).map(_.getValue)
        , Option(response.getIsScheduleChange).map(_.getValue)
        , response.getStatus.getValue
        , Target.withName(response.getTarget.value())
        , Option(response.getTktTimeLimit)
          .flatMap(data => Option(data.getValue))
          .map(_.toString)
          .map(DateTime.parse)
        , response.getUniqueID.getValue
      )
    }
}