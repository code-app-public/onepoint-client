package com.codeapp.result

import org.datacontract.schemas._2004._07.mystifly
import com.codeapp.entities.Extension._
import com.codeapp.entities.Target
import com.codeapp.entities.Target.Target

case class AirRemoveMessageQueueResult(conversationId: Option[String]
                                      , target: Target)

object AirRemoveMessageQueueResult {
  def apply(response: mystifly.AirRemoveMessageQueueRS): AirRemoveMessageQueueResult =
    validateResponse(response.isSuccess, response.getErrors) {
      AirRemoveMessageQueueResult(
        Option(response.getConversationId).map(_.getValue)
        , Target.withName(response.getTarget.value())
      )
    }
}
