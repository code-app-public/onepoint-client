package com.codeapp.result

import com.codeapp.entities._
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.mystifly
import com.codeapp.entities.Extension._

import scala.collection.JavaConverters._

case class AirLowFareSearchResult(conversationId: Option[String]
                                  , target: Target
                                  , pricedItineraries: Seq[PricedItinerary])

object AirLowFareSearchResult {
  def apply(response: mystifly.AirLowFareSearchRS): AirLowFareSearchResult =
    validateResponse(response.isSuccess, response.getErrors) {
      AirLowFareSearchResult(
        Option(response.getConversationId).map(_.getValue)
        , Target.withName(response.getTarget.value())
        , response.getPricedItineraries.getValue.getPricedItinerary.asScala.map(PricedItinerary.apply)
      )
    }
}
