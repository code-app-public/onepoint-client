package com.codeapp.result

import org.datacontract.schemas._2004._07.mystifly.AirMessageQueueRS
import com.codeapp.entities._
import com.codeapp.entities.Extension._
import com.codeapp.entities.Target
import com.codeapp.entities.Target.Target

import scala.collection.JavaConverters._

case class AirMessageQueueResult(conversationId: Option[String]
                                 , messageItems: Seq[MessageItem]
                                 , target: Target)

object AirMessageQueueResult {
  def apply(response: AirMessageQueueRS): AirMessageQueueResult =
    validateResponse(response.isSuccess, response.getErrors) {
      AirMessageQueueResult(
        Option(response.getConversationId).map(_.getValue)
        , response.getMessageItems.getValue.getMessageItem.asScala.map(MessageItem.apply)
        , Target.withName(response.getTarget.value())
      )
    }
}
