package com.codeapp.result

import com.codeapp.entities._
import com.codeapp.entities.Extension._
import com.codeapp.entities.Target
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.mystifly.AirRevalidateRS

import scala.collection.JavaConverters._

case class AirRevalidateResult(conversationId: Option[String] = None
                               , isValid: Boolean
                               , pricedItineraries: Seq[PricedItinerary]
                               , extraServices: ExtraService
                               , extraServices11: Option[ExtraService11]
                               , target: Target)

object AirRevalidateResult {
  def apply(response: AirRevalidateRS): AirRevalidateResult =
    validateResponse(response.isSuccess, response.getErrors) {
      AirRevalidateResult(
        Option(response.getConversationId).map(_.getValue)
        , response.isIsValid
        , response.getPricedItineraries.getValue.getPricedItinerary.asScala.map(PricedItinerary.apply)
        , ExtraService(response.getExtraServices.getValue)
        , Option(response.getExtraServices11).flatMap(d => Option(d.getValue)).map(ExtraService11.apply)
        , Target.withName(response.getTarget.value())
      )
    }
}

