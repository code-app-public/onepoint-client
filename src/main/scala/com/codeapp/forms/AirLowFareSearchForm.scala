package com.codeapp.forms

import com.codeapp.ObjectFactory
import org.datacontract.schemas._2004._07.mystifly
import com.codeapp.entities._
import com.codeapp.entities.Extension._
import com.codeapp.entities.PricingSourceType.PricingSourceType
import com.codeapp.entities.RequestOptions.RequestOptions
import com.codeapp.entities.Target
import com.codeapp.entities.Target.Target

case class AirLowFareSearchForm(conversationId: Option[String] = None
                                , sessionId: String
                                , target: Target
                                , originDestinationInformations: Seq[OriginDestinationInformation]
                                , passengerTypeQuantities: Seq[PassengerTypeQuantity]
                                , pricingSourceType: PricingSourceType
                                , requestOptions: RequestOptions
                                , travelPreferences: TravelPreferences
                                , isRefundable: Option[Boolean] = None
                                , isResidentFare: Option[Boolean] = None
                                , nearByAirports: Option[Boolean] = None)

object AirLowFareSearchForm {
  def toRequest(form: AirLowFareSearchForm): mystifly.AirLowFareSearchRQ = {
    val factory = ObjectFactory.mystifly
    val request = factory.createAirLowFareSearchRQ()

    val arrayOfOriginDestinationInformation = factory.createArrayOfOriginDestinationInformation()
    form.originDestinationInformations
      .map(OriginDestinationInformation.toRequest(factory))
      .foreach(arrayOfOriginDestinationInformation.getOriginDestinationInformation.add)
    request.setOriginDestinationInformations(factory.createAirLowFareSearchRQOriginDestinationInformations(arrayOfOriginDestinationInformation))

    val arrayOfPassengerTypeQuantity = factory.createArrayOfPassengerTypeQuantity()
    form.passengerTypeQuantities
        .map(PassengerTypeQuantity.toRequest(factory))
        .foreach(arrayOfPassengerTypeQuantity.getPassengerTypeQuantity.add)
    request.setPassengerTypeQuantities(factory.createAirLowFareSearchRQPassengerTypeQuantities(arrayOfPassengerTypeQuantity))

    request.setPricingSourceType(mystifly.PricingSourceType.fromValue(form.pricingSourceType.toString))
    request.setRequestOptions(mystifly.RequestOptions.fromValue(form.requestOptions.toString))
    request.setTravelPreferences(factory.createAirLowFareSearchRQTravelPreferences(TravelPreferences.toRequest(factory, form.travelPreferences)))
    request.setTarget(mystifly.Target.fromValue(form.target.toString))
    request.setSessionId(factory.createAirLowFareSearchRQSessionId(form.sessionId))

    form.conversationId.map(factory.createAirLowFareSearchRQConversationId).foreach(request.setConversationId)
    form.isRefundable.foreach(data => request.setIsRefundable(data))
    form.isResidentFare.foreach(data => request.setIsResidentFare(data))
    form.nearByAirports.foreach(data => request.setNearByAirports(data))

    request
  }
}











