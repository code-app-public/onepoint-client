package com.codeapp.forms

import com.codeapp.ObjectFactory
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.mystifly
import org.datacontract.schemas._2004._07.mystifly.AirTripDetailsRQ

case class AirTripDetailForm(conversationId: Option[String] = None
                             , sessionId: String
                             , target: Target
                             , sendOnlyTicketed: Option[Boolean] = None
                             , uniqueId: String)

object AirTripDetailForm {
  def toRequest(form: AirTripDetailForm): AirTripDetailsRQ = {
    val factory = ObjectFactory.mystifly
    val request = factory.createAirTripDetailsRQ()

    form.conversationId.map(factory.createAirTripDetailsRQConversationId).foreach(request.setConversationId)
    form.sendOnlyTicketed.foreach(data => request.setSendOnlyTicketed(data))
    request.setSessionId(factory.createAirTripDetailsRQSessionId(form.sessionId))
    request.setTarget(mystifly.Target.fromValue(form.target.toString))
    request.setUniqueID(factory.createAirTripDetailsRQUniqueID(form.uniqueId))

    request
  }
}
