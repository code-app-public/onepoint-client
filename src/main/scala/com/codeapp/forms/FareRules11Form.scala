package com.codeapp.forms

import com.codeapp.ObjectFactory
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.{mystifly, mystifly_onepoint}

case class FareRules11Form(conversationId: Option[String] = None
                           , sessionId: String
                           , fareSourceCode: String
                           , target: Target)

object FareRules11Form {
  def toRequest(form: FareRules11Form): mystifly_onepoint.AirRulesRQ = {
    val factory = ObjectFactory.mystiflyOnePoint
    val request = factory.createAirRulesRQ()

    form.conversationId.map(factory.createAirRulesRQConversationId).foreach(request.setConversationId)
    request.setSessionId(factory.createAirRulesRQSessionId(form.sessionId))
    request.setFareSourceCode(factory.createAirRulesRQFareSourceCode(form.fareSourceCode))
    request.setTarget(mystifly.Target.fromValue(form.target.toString))

    request
  }
}
