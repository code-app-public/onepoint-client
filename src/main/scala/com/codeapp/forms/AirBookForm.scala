package com.codeapp.forms

import com.codeapp.ObjectFactory
import org.datacontract.schemas._2004._07.mystifly
import com.codeapp.entities._
import com.codeapp.entities.Extension._
import com.codeapp.entities.Target
import com.codeapp.entities.Target.Target

case class AirBookForm(conversationId: Option[String] = None
                       , sessionId: String
                       , target: Target
                       , fareSourceCode: String
                       , travelerInfo: TravelerInfo
                       , lccHoldBooking: Option[Boolean] = None)

object AirBookForm {
  def toRequest(form: AirBookForm): mystifly.AirBookRQ = {
    val factory = ObjectFactory.mystifly
    val request = factory.createAirBookRQ()

    form.conversationId.map(factory.createAirBookRQConversationId).foreach(request.setConversationId)
    request.setFareSourceCode(factory.createAirBookRQFareSourceCode(form.fareSourceCode))
    form.lccHoldBooking.foreach(data => request.setLccHoldBooking(data))
    request.setSessionId(factory.createAirBookRQSessionId(form.sessionId))
    request.setTarget(mystifly.Target.fromValue(form.target.toString))
    request.setTravelerInfo(factory.createAirBookRQTravelerInfo(TravelerInfo.toRequest(factory, form.travelerInfo)))

    request
  }
}











