package com.codeapp.forms

import com.codeapp.ObjectFactory
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.mystifly

case class SessionCreateForm(conversationId: Option[String] = None
                             , target: Target
                             , accountNumber: String
                             , username: String
                             , password: String)

object SessionCreateForm {
  def toRequest(form: SessionCreateForm): mystifly.SessionCreateRQ = {
    val factory = ObjectFactory.mystifly
    val request = new mystifly.SessionCreateRQ()

    request.setAccountNumber(factory.createSessionCreateRQAccountNumber(form.accountNumber))
    request.setUserName(factory.createSessionCreateRQUserName(form.username))
    request.setPassword(factory.createSessionCreateRQPassword(form.password))
    request.setTarget(mystifly.Target.fromValue(form.target.toString))

    request
  }
}
