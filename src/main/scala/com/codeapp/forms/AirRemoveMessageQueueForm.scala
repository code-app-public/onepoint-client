package com.codeapp.forms

import com.codeapp.ObjectFactory
import org.datacontract.schemas._2004._07.mystifly
import com.codeapp.entities._
import com.codeapp.entities.Extension._
import com.codeapp.entities.Target
import com.codeapp.entities.Target.Target

case class AirRemoveMessageQueueForm(conversationId: Option[String] = None
                                     , sessionId: String
                                     , target: Target
                                     , items: Seq[Item])

object AirRemoveMessageQueueForm {
  def toRequest(form: AirRemoveMessageQueueForm): mystifly.AirRemoveMessageQueueRQ = {
    val factory = ObjectFactory.mystifly
    val request = factory.createAirRemoveMessageQueueRQ()

    form.conversationId.map(factory.createAirRemoveMessageQueueRQConversationId).foreach(request.setConversationId)

    val arrayOfItem = factory.createArrayOfItem()
    form.items.map(Item.toRequest(factory)).foreach(arrayOfItem.getItem.add)
    request.setItems(factory.createAirRemoveMessageQueueRQItems(arrayOfItem))

    request.setSessionId(factory.createAirRemoveMessageQueueRQSessionId(form.sessionId))
    request.setTarget(mystifly.Target.fromValue(form.target.toString))

    request
  }
}

