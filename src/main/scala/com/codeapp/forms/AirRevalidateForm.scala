package com.codeapp.forms

import com.codeapp.ObjectFactory
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.mystifly
import org.datacontract.schemas._2004._07.mystifly.AirRevalidateRQ

case class AirRevalidateForm(conversationId: Option[String] = None
                             , sessionId: String
                             , target: Target
                             , fareSourceCode: String)

object AirRevalidateForm {
  def toRequest(form: AirRevalidateForm): AirRevalidateRQ = {
    val factory = ObjectFactory.mystifly
    val request = factory.createAirRevalidateRQ()

    request.setFareSourceCode(factory.createAirRevalidateRQFareSourceCode(form.fareSourceCode))
    request.setSessionId(factory.createAirRevalidateRQSessionId(form.sessionId))
    request.setTarget(mystifly.Target.fromValue(form.target.toString))
    form.conversationId.map(factory.createAirRevalidateRQConversationId).foreach(request.setConversationId)

    request
  }
}
