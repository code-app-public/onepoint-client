package com.codeapp.forms

import com.codeapp.ObjectFactory
import com.codeapp.entities.QueueCategory.QueueCategory
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.mystifly

case class AirMessageQueueForm(conversationId: Option[String] = None
                               , sessionId: String
                               , target: Target
                               , categoryId: QueueCategory
                               , scheduleChangeType: Option[String] = None)

object AirMessageQueueForm {
  def toRequest(form: AirMessageQueueForm): mystifly.AirMessageQueueRQ = {
    val factory = ObjectFactory.mystifly
    val request = factory.createAirMessageQueueRQ()

    request.setCategoryId(mystifly.QueueCategory.fromValue(form.categoryId.toString))
    form.conversationId.map(factory.createAirMessageQueueRQConversationId).foreach(request.setConversationId)
    form.scheduleChangeType.map(factory.createAirMessageQueueRQScheduleChangeType).foreach(request.setScheduleChangeType)
    request.setSessionId(factory.createAirMessageQueueRQSessionId(form.sessionId))
    request.setTarget(mystifly.Target.fromValue(form.target.toString))

    request
  }
}
