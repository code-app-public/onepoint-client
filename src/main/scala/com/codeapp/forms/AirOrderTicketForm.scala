package com.codeapp.forms

import com.codeapp.ObjectFactory
import com.codeapp.entities.Target.Target
import org.datacontract.schemas._2004._07.mystifly

case class AirOrderTicketForm(conversationId: Option[String] = None
                              , sessionId: String
                              , target: Target
                              , uniqueId: String)

object AirOrderTicketForm {
  def toRequest(form: AirOrderTicketForm): mystifly.AirOrderTicketRQ = {
    val factory = ObjectFactory.mystifly
    val request = factory.createAirOrderTicketRQ()

    form.conversationId.map(factory.createAirOrderTicketRQConversationId).foreach(request.setConversationId)
    request.setSessionId(factory.createAirOrderTicketRQSessionId(form.sessionId))
    request.setTarget(mystifly.Target.fromValue(form.target.toString))
    request.setUniqueID(factory.createAirOrderTicketRQUniqueID(form.uniqueId))

    request
  }
}