package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object Target extends Enumeration {
  type Target = Value

  val PRODUCTION: Target = Value(mystifly.Target.PRODUCTION.value())
  val DEVELOPMENT: Target = Value(mystifly.Target.DEVELOPMENT.value())
  val TEST: Target = Value(mystifly.Target.TEST.value())
}
