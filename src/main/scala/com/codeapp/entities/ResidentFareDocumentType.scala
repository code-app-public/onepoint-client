package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class ResidentFareDocumentType(number: String, `type`: String, municipality: String)

object ResidentFareDocumentType {
  def toRequest(factory: mystifly.ObjectFactory)(form: ResidentFareDocumentType): mystifly.ResidentFareDocumentType = {
    val request = factory.createResidentFareDocumentType()

    request.setDocumentNumber(factory.createResidentFareDocumentTypeDocumentNumber(form.number))
    request.setDocumentType(factory.createResidentFareDocumentTypeDocumentType(form.`type`))
    request.setMunicipality(factory.createResidentFareDocumentTypeMunicipality(form.municipality))

    request
  }
}