package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class OriginDestinationOption(flightSegments: Seq[FlightSegment])

object OriginDestinationOption {
  def apply(response: mystifly.OriginDestinationOption): OriginDestinationOption =
    OriginDestinationOption(response.getFlightSegments.getValue.getFlightSegment.asScala.map(FlightSegment.apply))
}

