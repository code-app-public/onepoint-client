package com.codeapp.entities

import com.codeapp.entities.PassengerType.PassengerType
import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

case class Customer(age: Age
                    , dateOfBirth: DateTime
                    , email: String
                    , gender: String
                    , knownTravelerNo: String
                    , nameNumber: Int
                    , nationalId: String
                    , passengerNationality: String
                    , passengerType: PassengerType
                    , passportExpiresOn: Option[DateTime]
                    , passportIssuanceCountry: String
                    , passportNationality: String
                    , passportNumber: String
                    , paxName: PaxName
                    , phoneNumber: String
                    , postCode: String
                    , redressNo: String
                   )

object Customer {
  def apply(response: mystifly.Customer): Customer =
    Customer(
      Age(response.getAge.getValue)
      , DateTime.parse(response.getDateOfBirth.getValue.toString)
      , response.getEmailAddress.getValue
      , response.getGender.getValue
      , response.getKnownTravelerNo.getValue
      , response.getNameNumber
      , response.getNationalID.getValue
      , response.getPassengerNationality.getValue
      , PassengerType.withName(response.getPassengerType.value())
      , Option(response.getPassportExpiresOn).flatMap(d => Option(d.getValue)).map(_.toString).map(DateTime.parse)
      , response.getPassportIssuanceCountry.getValue
      , response.getPassportNationality.getValue
      , response.getPassportNumber.getValue
      , PaxName(response.getPaxName.getValue)
      , response.getPhoneNumber.getValue
      , response.getPostCode.getValue
      , response.getRedressNo.getValue
    )
}
