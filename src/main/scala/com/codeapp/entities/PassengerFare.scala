package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class PassengerFare(baseFare: Fare
                         , equivFare: Fare
                         , serviceTax: Option[Fare]
                         , surcharges: Seq[Surcharge]
                         , taxes: Seq[Tax]
                         , totalFare: Fare)

object PassengerFare {
  def apply(response: mystifly.PassengerFare): PassengerFare =
    PassengerFare(
      Fare(response.getBaseFare.getValue)
      , Fare(response.getEquivFare.getValue)
      , Option(response.getServiceTax).map(data => Fare(data.getValue))
      , response.getSurcharges.getValue.getSurcharge.asScala.map(Surcharge.apply)
      , response.getTaxes.getValue.getTax.asScala.map(Tax.apply)
      , Fare(response.getTotalFare.getValue)
    )
}