package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class TripDetailsPassengerFare(airportTaxBreakup: Option[String]
                                    , equiFare: Fare
                                    , serviceTax: Option[Fare]
                                    , tax: Fare
                                    , totalFare: Fare)

object TripDetailsPassengerFare {
  def apply(response: mystifly.TripDetailsPassengerFare): TripDetailsPassengerFare =
    TripDetailsPassengerFare(
      Option(response.getAirportTaxBreakUp).map(_.getValue)
      , Fare(response.getEquiFare.getValue)
      , Option(response.getServiceTax).map(_.getValue).map(Fare.apply)
      , Fare(response.getTax.getValue)
      , Fare(response.getTotalFare.getValue)
    )
}
