package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

case class StopQuantityInfo(arrivalDateTime: DateTime
                            , departureDateTime: DateTime
                            , duration: Int
                            , locationCode: String
                            , stopOverEquipment: Option[String])

object StopQuantityInfo {
  def apply(response: mystifly.StopQuantityInfo): StopQuantityInfo =
    StopQuantityInfo(
      DateTime.parse(response.getArrivalDateTime.toString)
      , DateTime.parse(response.getDepartureDateTime.toString)
      , response.getDuration
      , response.getLocationCode.getValue
      , Option(response.getStopOverEquipment).map(_.getValue)
    )
}

