package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class Age(months: String, years: String)

object Age {
  def apply(response: mystifly.Age): Age =
    Age(response.getMonths.getValue, response.getYears.getValue)
}
