package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class PTCFareBreakdown(baggageInfo: BaggageInfo
                            , cabinBaggageInfo: Option[CabinBaggageInfo]
                            , fareBasisCodes: Seq[String]
                            , passengerFare: PassengerFare
                            , passengerTypeQuantity: PassengerTypeQuantity
                            , penaltiesInfo: Option[Seq[Penalty]])

object PTCFareBreakdown {
  def apply(response: mystifly.PTCFareBreakdown): PTCFareBreakdown =
    PTCFareBreakdown(
      BaggageInfo(response.getBaggageInfo.getValue)
      , Option(response.getCabinBaggageInfo).map(data => CabinBaggageInfo(data.getValue))
      , response.getFareBasisCodes.getValue.getString.asScala
      , PassengerFare(response.getPassengerFare.getValue)
      , PassengerTypeQuantity(
        PassengerType.withName(response.getPassengerTypeQuantity.getValue.getCode.value())
        , response.getPassengerTypeQuantity.getValue.getQuantity
      )
      , Option(response.getPenaltiesInfo).map(_.getValue.getPenalty.asScala.map(Penalty.apply))
    )
}