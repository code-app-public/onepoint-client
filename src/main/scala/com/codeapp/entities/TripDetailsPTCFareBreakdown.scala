package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class TripDetailsPTCFareBreakdown(baggageInfo: BaggageInfo
                                      , cabinBaggageInfo: Option[CabinBaggageInfo]
                                      , passengerTypeQuantity: PassengerTypeQuantity
                                      , tripDetailsPassengerFare: TripDetailsPassengerFare)

object TripDetailsPTCFareBreakdown {
  def apply(response: mystifly.TripDetailsPTCFareBreakdown): TripDetailsPTCFareBreakdown =
    TripDetailsPTCFareBreakdown(
      BaggageInfo(response.getBaggageInfo.getValue)
      , Option(response.getCabinBaggageInfo).map(_.getValue).map(CabinBaggageInfo.apply)
      , PassengerTypeQuantity(response.getPassengerTypeQuantity.getValue)
      , TripDetailsPassengerFare(response.getTripDetailsPassengerFare.getValue)
    )
}

