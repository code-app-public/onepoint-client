package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object FareType extends Enumeration {
  type FareType = Value

  val DEFAULT: FareType = Value(mystifly.FareType.DEFAULT.value())
  val PUBLIC: FareType = Value(mystifly.FareType.PUBLIC.value())
  val PRIVATE: FareType = Value(mystifly.FareType.PRIVATE.value())
  val WEB_FARE: FareType = Value(mystifly.FareType.WEB_FARE.value())
}
