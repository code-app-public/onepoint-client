package com.codeapp.entities

import com.codeapp.entities.Extension._
import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

case class Passport(country: String, expiryDate: DateTime, number: String)

object Passport {
  def toRequest(factory: mystifly.ObjectFactory)(form: Passport): mystifly.Passport = {
    val request = factory.createPassport()

    request.setCountry(factory.createPassportCountry(form.country))
    request.setExpiryDate(toXMLGregorianCalendar(form.expiryDate))
    request.setPassportNumber(factory.createPassportPassportNumber(form.number))

    request
  }
}