package com.codeapp.entities

import onepointentities.onepoint.mystifly

import scala.collection.JavaConverters._

case class ExtraService11(services: Seq[Service11])

object ExtraService11 {
  def apply(response: mystifly.ExtraService): ExtraService11 =
    ExtraService11(
      response.getServices.getValue.getService.asScala.map(Service11.apply)
    )
}

case class Service11(
                    behavior: String
                    , checkInType: String
                    , description: String
                    , flightDesignator: String
                    , isMandatory: Boolean
                    , relation: String
                    , serviceCost: ServiceCost
                    , serviceId: String
                    , `type`: String
                    )

object Service11 {
  def apply(response: mystifly.Service): Service11 =
    Service11(
      response.getBehavior.getValue
      , response.getCheckInType.getValue
      , response.getDescription.getValue
      , response.getFlightDesignator.getValue
      , response.isIsMandatory
      , response.getRelation.getValue
      , ServiceCost(response.getServiceCost.getValue)
      , response.getServiceId.getValue
      , response.getType.getValue
    )
}
