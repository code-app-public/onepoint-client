package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

import scala.collection.JavaConverters._

case class ReservationItem(airEquipmentType: String
                           , airlinePNR: String
                           , arrivalAirportLocationCode: String
                           , arrivalDateTime: DateTime
                           , arrivalTerminal: String
                           , baggage: String
                           , cabinClassText: String
                           , departureAirportLocationCode: String
                           , departureDateTime: DateTime
                           , departureTerminal: String
                           , flightNumber: String
                           , itemPRH: Int
                           , journeyDuration: String
                           , marketingAirlineCode: String
                           , numberInParty: Int
                           , operatingAirlineCode: String
                           , resBookDesigCode: String
                           , resBookDesigText: String
                           , stopQuantity: Int
                           , stopQuantityInformations: Option[Seq[StopQuantityInfo]]
                          )

object ReservationItem {
  def apply(response: mystifly.ReservationItem): ReservationItem =
    ReservationItem(
      response.getAirEquipmentType.getValue
      , response.getAirlinePNR.getValue
      , response.getArrivalAirportLocationCode.getValue
      , DateTime.parse(response.getArrivalDateTime.toString)
      , response.getArrivalTerminal.getValue
      , response.getBaggage.getValue
      , response.getCabinClassText.getValue
      , response.getDepartureAirportLocationCode.getValue
      , DateTime.parse(response.getDepartureDateTime.toString)
      , response.getDepartureTerminal.getValue
      , response.getFlightNumber.getValue
      , response.getItemRPH
      , response.getJourneyDuration.getValue
      , response.getMarketingAirlineCode.getValue
      , response.getNumberInParty
      , response.getOperatingAirlineCode.getValue
      , response.getResBookDesigCode.getValue
      , response.getResBookDesigText.getValue
      , response.getStopQuantity
      , Option(response.getStopQuantityInformations).map(_.getValue.getStopQuantityInfo.asScala.map(StopQuantityInfo.apply))
    )
}

