package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class TravelItinerary(bookingNotes: Option[Seq[String]] = None
                           , bookingStatus: String
                           , destination: String
                           , extraService: Option[ExtraService] = None
                           , extraService11: Option[ExtraService11] = None
                           , fareType: String
                           , itineraryInfo: ItineraryInfo
                           , origin: String
                           , ticketStatus: String
                           , uniqueId: String
                           , isCrossBorderIndicator: Boolean
                           , isCommissionable: Boolean
                           , isMOFare: Boolean)

object TravelItinerary {
  def apply(response: mystifly.TravelItinerary): TravelItinerary =
    TravelItinerary(
      Option(response.getBookingNotes).flatMap(d => Option(d.getValue)).map(_.getString.asScala)
      , response.getBookingStatus.getValue
      , response.getDestination.getValue
      , Option(response.getExtraServices).map(_.getValue).map(ExtraService.apply)
      , Option(response.getExtraServices11).map(_.getValue).map(ExtraService11.apply)
      , response.getFareType.getValue
      , ItineraryInfo(response.getItineraryInfo.getValue)
      , response.getOrigin.getValue
      , response.getTicketStatus.getValue
      , response.getUniqueID.getValue
      , response.isCrossBorderIndicator
      , response.isIsCommissionable
      , response.isIsMOFare
    )
}
