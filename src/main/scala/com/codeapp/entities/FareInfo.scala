package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class FareInfo(reference: String)

object FareInfo {
  def apply(response: mystifly.FareInfo): FareInfo =
    FareInfo(response.getFareReference.getValue)
}
