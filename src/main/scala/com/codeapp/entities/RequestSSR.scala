package com.codeapp.entities

import com.codeapp.entities.SSRCode.SSRCode
import org.datacontract.schemas._2004._07.mystifly

case class RequestSSR(freeText: String, ssrCodes: SSRCode)

object RequestSSR {
  def toRequest(factory: mystifly.ObjectFactory, form: RequestSSR): mystifly.RequestSSR = {
    val request = factory.createRequestSSR()

    request.setFreeText(factory.createRequestSSRFreeText(form.freeText))
    request.setSSRCode(mystifly.SSRCodes.fromValue(form.ssrCodes.toString))

    request
  }
}

