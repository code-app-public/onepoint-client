package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object PassengerType extends Enumeration {
  type PassengerType = Value

  val ADULT: PassengerType = Value(mystifly.PassengerType.ADT.value())
  val CHILD: PassengerType = Value(mystifly.PassengerType.CHD.value())
  val INFANT: PassengerType = Value(mystifly.PassengerType.INF.value())
}

