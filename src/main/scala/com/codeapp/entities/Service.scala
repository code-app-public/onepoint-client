package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class Service(extraServiceId: String
                   , serviceCost: ServiceCost
                   , serviceDescription: ServiceDescription)

object Service {
  def apply(response: mystifly.Service): Service =
    Service(
      response.getExtraServiceId.getValue
      , ServiceCost(response.getServiceCost.getValue)
      , ServiceDescription(response.getServiceDescription.getValue)
    )
}
