package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object PricingSourceType extends Enumeration {
  type PricingSourceType = Value

  val DEFAULT: PricingSourceType = Value(mystifly.PricingSourceType.DEFAULT.value())
  val PUBLIC: PricingSourceType = Value(mystifly.PricingSourceType.PUBLIC.value())
  val PRIVATE: PricingSourceType = Value(mystifly.PricingSourceType.PRIVATE.value())
  val ALL: PricingSourceType = Value(mystifly.PricingSourceType.ALL.value())
}
