package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class SeatsRemaining(belowMinimum: Boolean, number: Int)

object SeatsRemaining {
  def apply(response: mystifly.SeatsRemaining): SeatsRemaining =
    SeatsRemaining(response.isBelowMinimum, response.getNumber)
}

