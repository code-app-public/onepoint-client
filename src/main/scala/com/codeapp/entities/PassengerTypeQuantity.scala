package com.codeapp.entities

import com.codeapp.entities.PassengerType.PassengerType
import org.datacontract.schemas._2004._07.mystifly


case class PassengerTypeQuantity(code: PassengerType, quantity: Int)

object PassengerTypeQuantity {

  def apply(response: mystifly.PassengerTypeQuantity): PassengerTypeQuantity =
    PassengerTypeQuantity(
      PassengerType.withName(response.getCode.value())
      , response.getQuantity
    )

  def toRequest(factory: mystifly.ObjectFactory)(form: PassengerTypeQuantity): mystifly.PassengerTypeQuantity = {
    val passengerTypeQuantity = factory.createPassengerTypeQuantity()

    passengerTypeQuantity.setCode(mystifly.PassengerType.fromValue(form.code.toString))
    passengerTypeQuantity.setQuantity(form.quantity)

    passengerTypeQuantity
  }

}
