package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class BaggageInfo(baggage: Seq[String])

object BaggageInfo {
  def apply(response: mystifly.BaggageInfo): BaggageInfo =
    BaggageInfo(response.getBaggage.asScala)
}