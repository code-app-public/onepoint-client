package com.codeapp.entities

class OnePointException(val code: String, message: String) extends Exception(message) {
  import OnePointErrorCode._

  def isSessionIdInvalid: Boolean = {
    OnePointErrorCode.withName(code) match {
      case IntelliFarePricer.InvalidSessionId |
           FareRules.InvalidSessionId |
           AirRevalidate.InvalidSessionId |
           IntelliBook.InvalidSessionId |
           CancelBooking.InvalidSessionId |
           SeatMap.InvalidSessionId |
           SeatReserve.InvalidSessionId |
           OrderTicket.InvalidSessionId |
           AirQuote.InvalidSessionId |
           TripDetails.InvalidSessionId |
           MessageQueue.InvalidSessionId |
           RemoveMessageQueue.InvalidSessionId |
           AddBookingNotes.InvalidSessionId |
           AirBookingData.InvalidSessionId |
           MultiAirRevalidate.InvalidSessionId |
           MultiAirBook.InvalidSessionId |
           IntelliBestBuy.InvalidSessionId |
           OnePointPayment.InvalidSessionId => true
      case _ => false
    }
  }
}

object OnePointErrorCode extends Enumeration {
  type OnePointErrorCode = Value

  object Authenticate {
    val TargetCannotBeNull: OnePointErrorCode = Value("ERCRS001")
    val UserNameCannotBeNull: OnePointErrorCode = Value("ERCRS002")
    val PasswordCannotBeNull: OnePointErrorCode = Value("ERCRS003")
    val AccountNumberCannotBeNull: OnePointErrorCode = Value("ERCRS004")
    val InvalidLoginCredential: OnePointErrorCode = Value("ERCRS005")
  }

  object IntelliFarePricer {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERSER001")
    val InvalidSessionId: OnePointErrorCode = Value("ERSER002")
//    val _: OnePointErrorCode = Value("ERSER003")
//    val _: OnePointErrorCode = Value("ERSER004")
//    val _: OnePointErrorCode = Value("ERSER005")
//    val _: OnePointErrorCode = Value("ERSER006")
//    val _: OnePointErrorCode = Value("ERSER007")
//    val _: OnePointErrorCode = Value("ERSER008")
//    val _: OnePointErrorCode = Value("ERSER009")
//    val _: OnePointErrorCode = Value("ERSER010")
//    val _: OnePointErrorCode = Value("ERSER011")
//    val _: OnePointErrorCode = Value("ERSER012")
//    val _: OnePointErrorCode = Value("ERSER013")
//    val _: OnePointErrorCode = Value("ERSER014")
//    val _: OnePointErrorCode = Value("ERSER015")
//    val _: OnePointErrorCode = Value("ERSER016")
//    val _: OnePointErrorCode = Value("ERSER017")
//    val _: OnePointErrorCode = Value("ERSER018")
//    val _: OnePointErrorCode = Value("ERSER019")
//    val _: OnePointErrorCode = Value("ERSER020")
//    val _: OnePointErrorCode = Value("ERSER021")
//    val _: OnePointErrorCode = Value("ERSER022")
//    val _: OnePointErrorCode = Value("ERSER023")
//    val _: OnePointErrorCode = Value("ERSER024")
//    val _: OnePointErrorCode = Value("ERSER025")
//    val _: OnePointErrorCode = Value("ERSER026")
//    val _: OnePointErrorCode = Value("ERSER027")
  }

  object FareRules {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERFRU001")
//    val _: OnePointErrorCode = Value("ERFRU002")
//    val _: OnePointErrorCode = Value("ERFRU003")
//    val _: OnePointErrorCode = Value("ERFRU004")
//    val _: OnePointErrorCode = Value("ERFRU005")
//    val _: OnePointErrorCode = Value("ERFRU006")
//    val _: OnePointErrorCode = Value("ERFRU007")
//    val _: OnePointErrorCode = Value("ERFRU008")
//    val _: OnePointErrorCode = Value("ERFRU009")
//    val _: OnePointErrorCode = Value("ERFRU010")
//    val _: OnePointErrorCode = Value("ERFRU011")
//    val _: OnePointErrorCode = Value("ERFRU012")
    val InvalidSessionId: OnePointErrorCode = Value("ERFRU013")
//    val _: OnePointErrorCode = Value("ERFRU014")
  }

  object AirRevalidate {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERREV001")
    val InvalidSessionId: OnePointErrorCode = Value("ERREV002")
//    val _: OnePointErrorCode = Value("ERREV003")
//    val _: OnePointErrorCode = Value("ERREV004")
//    val _: OnePointErrorCode = Value("ERREV005")
  }

  object IntelliBook {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERBUK001")
    val InvalidSessionId: OnePointErrorCode = Value("ERBUK002")
//    val _: OnePointErrorCode = Value("ERBUK003")
//    val _: OnePointErrorCode = Value("ERBUK004")
//    val _: OnePointErrorCode = Value("ERBUK005")
//    val _: OnePointErrorCode = Value("ERBUK006")
//    val _: OnePointErrorCode = Value("ERBUK007")
//    val _: OnePointErrorCode = Value("ERBUK008")
//    val _: OnePointErrorCode = Value("ERBUK009")
//    val _: OnePointErrorCode = Value("ERBUK010")
//    val _: OnePointErrorCode = Value("ERBUK011")
//    val _: OnePointErrorCode = Value("ERBUK012")
//    val _: OnePointErrorCode = Value("ERBUK013")
//    val _: OnePointErrorCode = Value("ERBUK014")
//    val _: OnePointErrorCode = Value("ERBUK015")
//    val _: OnePointErrorCode = Value("ERBUK016")
//    val _: OnePointErrorCode = Value("ERBUK017")
//    val _: OnePointErrorCode = Value("ERBUK018")
//    val _: OnePointErrorCode = Value("ERBUK019")
//    val _: OnePointErrorCode = Value("ERBUK020")
//    val _: OnePointErrorCode = Value("ERBUK021")
//    val _: OnePointErrorCode = Value("ERBUK022")
//    val _: OnePointErrorCode = Value("ERBUK023")
//    val _: OnePointErrorCode = Value("ERBUK024")
//    val _: OnePointErrorCode = Value("ERBUK025")
//    val _: OnePointErrorCode = Value("ERBUK026")
//    val _: OnePointErrorCode = Value("ERBUK027")
//    val _: OnePointErrorCode = Value("ERBUK028")
//    val _: OnePointErrorCode = Value("ERBUK029")
//    val _: OnePointErrorCode = Value("ERBUK030")
//    val _: OnePointErrorCode = Value("ERBUK031")
//    val _: OnePointErrorCode = Value("ERBUK032")
//    val _: OnePointErrorCode = Value("ERBUK033")
//    val _: OnePointErrorCode = Value("ERBUK034")
//    val _: OnePointErrorCode = Value("ERBUK035")
//    val _: OnePointErrorCode = Value("ERBUK036")
//    val _: OnePointErrorCode = Value("ERBUK037")
//    val _: OnePointErrorCode = Value("ERBUK038")
//    val _: OnePointErrorCode = Value("ERBUK039")
//    val _: OnePointErrorCode = Value("ERBUK040")
//    val _: OnePointErrorCode = Value("ERBUK041")
//    val _: OnePointErrorCode = Value("ERBUK042")
//    val _: OnePointErrorCode = Value("ERBUK043")
//    val _: OnePointErrorCode = Value("ERBUK044")
//    val _: OnePointErrorCode = Value("ERBUK045")
//    val _: OnePointErrorCode = Value("ERBUK046")
//    val _: OnePointErrorCode = Value("ERBUK047")
//    val _: OnePointErrorCode = Value("ERBUK048")
//    val _: OnePointErrorCode = Value("ERBUK049")
//    val _: OnePointErrorCode = Value("ERBUK050")
//    val _: OnePointErrorCode = Value("ERBUK051")
//    val _: OnePointErrorCode = Value("ERBUK052")
//    val _: OnePointErrorCode = Value("ERBUK053")
//    val _: OnePointErrorCode = Value("ERBUK054")
//    val _: OnePointErrorCode = Value("ERBUK055")
//    val _: OnePointErrorCode = Value("ERBUK056")
//    val _: OnePointErrorCode = Value("ERBUK057")
//    val _: OnePointErrorCode = Value("ERBUK058")
//    val _: OnePointErrorCode = Value("ERBUK059")
//    val _: OnePointErrorCode = Value("ERBUK060")
//    val _: OnePointErrorCode = Value("ERBUK061")
//    val _: OnePointErrorCode = Value("ERBUK062")
//    val _: OnePointErrorCode = Value("ERBUK063")
//    val _: OnePointErrorCode = Value("ERBUK064")
//    val _: OnePointErrorCode = Value("ERBUK065")
//    val _: OnePointErrorCode = Value("ERBUK066")
//    val _: OnePointErrorCode = Value("ERBUK067")
//    val _: OnePointErrorCode = Value("ERBUK068")
//    val _: OnePointErrorCode = Value("ERBUK069")
//    val _: OnePointErrorCode = Value("ERBUK070")
//    val _: OnePointErrorCode = Value("ERBUK071")
//    val _: OnePointErrorCode = Value("ERBUK072")
//    val _: OnePointErrorCode = Value("ERBUK073")
//    val _: OnePointErrorCode = Value("ERBUK074")
//    val _: OnePointErrorCode = Value("ERBUK075")
//    val _: OnePointErrorCode = Value("ERBUK076")
//    val _: OnePointErrorCode = Value("ERBUK077")
//    val _: OnePointErrorCode = Value("ERBUK078")
//    val _: OnePointErrorCode = Value("ERBUK079")
//    val _: OnePointErrorCode = Value("ERBUK080")
//    val _: OnePointErrorCode = Value("ERBUK081")
//    val _: OnePointErrorCode = Value("ERBUK082")
//    val _: OnePointErrorCode = Value("ERBUK083")
//    val _: OnePointErrorCode = Value("ERBUK084")
//    val _: OnePointErrorCode = Value("ERBUK085")
//    val _: OnePointErrorCode = Value("ERBUK086")
//    val _: OnePointErrorCode = Value("ERBUK087")
//    val _: OnePointErrorCode = Value("ERBUK088")
//    val _: OnePointErrorCode = Value("ERBUK089")
//    val _: OnePointErrorCode = Value("ERBUK090")
//    val _: OnePointErrorCode = Value("ERBUK091")
//    val _: OnePointErrorCode = Value("ERBUK092")
//    val _: OnePointErrorCode = Value("ERBUK093")
//    val _: OnePointErrorCode = Value("ERBUK094")
//    val _: OnePointErrorCode = Value("ERBUK095")
//    val _: OnePointErrorCode = Value("ERBUK096")
//    val _: OnePointErrorCode = Value("ERBUK097")
//    val _: OnePointErrorCode = Value("ERBUK098")
//    val _: OnePointErrorCode = Value("ERBUK099")
//    val _: OnePointErrorCode = Value("ERBUK100")
//    val _: OnePointErrorCode = Value("ERBUK101")
//    val _: OnePointErrorCode = Value("ERBUK102")
  }

  object CancelBooking {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERCBK001")
    val InvalidSessionId: OnePointErrorCode = Value("ERCBK002")
//    val _: OnePointErrorCode = Value("ERCBK003")
//    val _: OnePointErrorCode = Value("ERCBK004")
//    val _: OnePointErrorCode = Value("ERCBK005")
//    val _: OnePointErrorCode = Value("ERCBK006")
//    val _: OnePointErrorCode = Value("ERCBK007")
  }

  object SeatMap {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERSMP001")
    val InvalidSessionId: OnePointErrorCode = Value("ERSMP002")
//    val _: OnePointErrorCode = Value("ERSMP003")
//    val _: OnePointErrorCode = Value("ERSMP004")
//    val _: OnePointErrorCode = Value("ERSMP005")
//    val _: OnePointErrorCode = Value("ERSMP006")
//    val _: OnePointErrorCode = Value("ERSMP007")
//    val _: OnePointErrorCode = Value("ERSMP008")
//    val _: OnePointErrorCode = Value("ERSMP009")
//    val _: OnePointErrorCode = Value("ERSMP010")
//    val _: OnePointErrorCode = Value("ERSMP011")
//    val _: OnePointErrorCode = Value("ERSMP012")
//    val _: OnePointErrorCode = Value("ERSMP013")
//    val _: OnePointErrorCode = Value("ERSMP014")
  }

  object SeatReserve {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERSRV001")
    val InvalidSessionId: OnePointErrorCode = Value("ERSRV002")
//    val _: OnePointErrorCode = Value("ERSRV003")
//    val _: OnePointErrorCode = Value("ERSRV004")
//    val _: OnePointErrorCode = Value("ERSRV005")
//    val _: OnePointErrorCode = Value("ERSRV006")
//    val _: OnePointErrorCode = Value("ERSRV007")
//    val _: OnePointErrorCode = Value("ERSRV008")
//    val _: OnePointErrorCode = Value("ERSRV009")
//    val _: OnePointErrorCode = Value("ERSRV010")
//    val _: OnePointErrorCode = Value("ERSRV011")
//    val _: OnePointErrorCode = Value("ERSRV012")
//    val _: OnePointErrorCode = Value("ERSRV013")
//    val _: OnePointErrorCode = Value("ERSRV014")
//    val _: OnePointErrorCode = Value("ERSRV015")
//    val _: OnePointErrorCode = Value("ERSRV016")
//    val _: OnePointErrorCode = Value("ERSRV017")
  }

  object OrderTicket {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("EROTK001")
    val InvalidSessionId: OnePointErrorCode = Value("EROTK002")
//    val _: OnePointErrorCode = Value("EROTK003")
//    val _: OnePointErrorCode = Value("EROTK004")
//    val _: OnePointErrorCode = Value("EROTK005")
//    val _: OnePointErrorCode = Value("EROTK006")
//    val _: OnePointErrorCode = Value("EROTK007")
//    val _: OnePointErrorCode = Value("EROTK008")
//    val _: OnePointErrorCode = Value("EROTK009")
  }

  object AirQuote {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERAQT001")
    val InvalidSessionId: OnePointErrorCode = Value("ERAQT002")
//    val _: OnePointErrorCode = Value("ERAQT003")
//    val _: OnePointErrorCode = Value("ERAQT004")
//    val _: OnePointErrorCode = Value("ERAQT005")
//    val _: OnePointErrorCode = Value("ERAQT006")
//    val _: OnePointErrorCode = Value("ERAQT007")
//    val _: OnePointErrorCode = Value("ERAQT008")
//    val _: OnePointErrorCode = Value("ERAQT009")
//    val _: OnePointErrorCode = Value("ERAQT010")
//    val _: OnePointErrorCode = Value("ERAQT011")
//    val _: OnePointErrorCode = Value("ERAQT012")
//    val _: OnePointErrorCode = Value("ERAQT013")
//    val _: OnePointErrorCode = Value("ERAQT014")
//    val _: OnePointErrorCode = Value("ERAQT015")
//    val _: OnePointErrorCode = Value("ERAQT016")
//    val _: OnePointErrorCode = Value("ERAQT017")
//    val _: OnePointErrorCode = Value("ERAQT018")
//    val _: OnePointErrorCode = Value("ERAQT019")
//    val _: OnePointErrorCode = Value("ERAQT020")
//    val _: OnePointErrorCode = Value("ERAQT021")
//    val _: OnePointErrorCode = Value("ERAQT022")
//    val _: OnePointErrorCode = Value("ERAQT023")
//    val _: OnePointErrorCode = Value("ERAQT024")
//    val _: OnePointErrorCode = Value("ERAQT025")
//    val _: OnePointErrorCode = Value("ERAQT026")
//    val _: OnePointErrorCode = Value("ERAQT027")
//    val _: OnePointErrorCode = Value("ERAQT028")
//    val _: OnePointErrorCode = Value("ERAQT029")
//    val _: OnePointErrorCode = Value("ERAQT030")
  }

  object TripDetails {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERTDT001")
    val InvalidSessionId: OnePointErrorCode = Value("ERTDT002")
//    val _: OnePointErrorCode = Value("ERTDT003")
//    val _: OnePointErrorCode = Value("ERTDT004")
//    val _: OnePointErrorCode = Value("ERTDT005")
//    val _: OnePointErrorCode = Value("ERTDT006")
  }

  object MessageQueue {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERMQU001")
//    val _: OnePointErrorCode = Value("ERMQU002")
    val InvalidSessionId: OnePointErrorCode = Value("ERMQU003")
  }

  object RemoveMessageQueue {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERRMQ001")
//    val _: OnePointErrorCode = Value("ERRMQ002")
//    val _: OnePointErrorCode = Value("ERRMQ003")
//    val _: OnePointErrorCode = Value("ERRMQ004")
//    val _: OnePointErrorCode = Value("ERRMQ005")
    val InvalidSessionId: OnePointErrorCode = Value("ERRMQ006")
  }

  object AddBookingNotes {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERABN001")
//    val _: OnePointErrorCode = Value("ERABN002")
//    val _: OnePointErrorCode = Value("ERABN003")
//    val _: OnePointErrorCode = Value("ERABN004")
//    val _: OnePointErrorCode = Value("ERABN005")
    val InvalidSessionId: OnePointErrorCode = Value("ERABN006")
  }

  object AirBookingData {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERABD001")
    val InvalidSessionId: OnePointErrorCode = Value("ERABD002")
//    val _: OnePointErrorCode = Value("ERABD003")
//    val _: OnePointErrorCode = Value("ERABD004")
//    val _: OnePointErrorCode = Value("ERABD005")
  }

  object MultiAirRevalidate {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERMAR001")
    val InvalidSessionId: OnePointErrorCode = Value("ERMAR002")
//    val _: OnePointErrorCode = Value("ERMAR003")
//    val _: OnePointErrorCode = Value("ERMAR004")
//    val _: OnePointErrorCode = Value("ERMAR005")
//    val _: OnePointErrorCode = Value("ERMAR006")
//    val _: OnePointErrorCode = Value("ERMAR007")
//    val _: OnePointErrorCode = Value("ERMAR008")
  }

  object MultiAirBook {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERMAB001")
    val InvalidSessionId: OnePointErrorCode = Value("ERMAB002")
//    val _: OnePointErrorCode = Value("ERMAB003")
//    val _: OnePointErrorCode = Value("ERMAB004")
//    val _: OnePointErrorCode = Value("ERMAB005")
//    val _: OnePointErrorCode = Value("ERMAB006")
//    val _: OnePointErrorCode = Value("ERMAB007")
//    val _: OnePointErrorCode = Value("ERMAB008")
//    val _: OnePointErrorCode = Value("ERMAB009")
//    val _: OnePointErrorCode = Value("ERMAB010")
//    val _: OnePointErrorCode = Value("ERMAB011")
//    val _: OnePointErrorCode = Value("ERMAB012")
//    val _: OnePointErrorCode = Value("ERMAB013")
//    val _: OnePointErrorCode = Value("ERMAB014")
//    val _: OnePointErrorCode = Value("ERMAB015")
//    val _: OnePointErrorCode = Value("ERMAB016")
//    val _: OnePointErrorCode = Value("ERMAB017")
//    val _: OnePointErrorCode = Value("ERMAB018")
//    val _: OnePointErrorCode = Value("ERMAB019")
//    val _: OnePointErrorCode = Value("ERMAB020")
//    val _: OnePointErrorCode = Value("ERMAB021")
//    val _: OnePointErrorCode = Value("ERMAB022")
//    val _: OnePointErrorCode = Value("ERMAB023")
//    val _: OnePointErrorCode = Value("ERMAB024")
//    val _: OnePointErrorCode = Value("ERMAB025")
//    val _: OnePointErrorCode = Value("ERMAB026")
//    val _: OnePointErrorCode = Value("ERMAB027")
//    val _: OnePointErrorCode = Value("ERMAB028")
//    val _: OnePointErrorCode = Value("ERMAB029")
//    val _: OnePointErrorCode = Value("ERMAB030")
//    val _: OnePointErrorCode = Value("ERMAB031")
//    val _: OnePointErrorCode = Value("ERMAB032")
//    val _: OnePointErrorCode = Value("ERMAB033")
//    val _: OnePointErrorCode = Value("ERMAB034")
//    val _: OnePointErrorCode = Value("ERMAB035")
//    val _: OnePointErrorCode = Value("ERMAB036")
//    val _: OnePointErrorCode = Value("ERMAB037")
//    val _: OnePointErrorCode = Value("ERMAB038")
//    val _: OnePointErrorCode = Value("ERMAB039")
//    val _: OnePointErrorCode = Value("ERMAB040")
//    val _: OnePointErrorCode = Value("ERMAB041")
//    val _: OnePointErrorCode = Value("ERMAB042")
//    val _: OnePointErrorCode = Value("ERMAB043")
//    val _: OnePointErrorCode = Value("ERMAB044")
//    val _: OnePointErrorCode = Value("ERMAB045")
//    val _: OnePointErrorCode = Value("ERMAB046")
//    val _: OnePointErrorCode = Value("ERMAB047")
//    val _: OnePointErrorCode = Value("ERMAB048")
//    val _: OnePointErrorCode = Value("ERMAB049")
//    val _: OnePointErrorCode = Value("ERMAB050")
//    val _: OnePointErrorCode = Value("ERMAB051")
//    val _: OnePointErrorCode = Value("ERMAB052")
//    val _: OnePointErrorCode = Value("ERMAB053")
//    val _: OnePointErrorCode = Value("ERMAB054")
//    val _: OnePointErrorCode = Value("ERMAB055")
//    val _: OnePointErrorCode = Value("ERMAB056")
//    val _: OnePointErrorCode = Value("ERMAB057")
//    val _: OnePointErrorCode = Value("ERMAB058")
//    val _: OnePointErrorCode = Value("ERMAB059")
//    val _: OnePointErrorCode = Value("ERMAB060")
//    val _: OnePointErrorCode = Value("ERMAB061")
//    val _: OnePointErrorCode = Value("ERMAB062")
//    val _: OnePointErrorCode = Value("ERMAB063")
//    val _: OnePointErrorCode = Value("ERMAB064")
//    val _: OnePointErrorCode = Value("ERMAB065")
//    val _: OnePointErrorCode = Value("ERMAB066")
//    val _: OnePointErrorCode = Value("ERMAB067")
//    val _: OnePointErrorCode = Value("ERMAB068")
//    val _: OnePointErrorCode = Value("ERMAB069")
//    val _: OnePointErrorCode = Value("ERMAB070")
//    val _: OnePointErrorCode = Value("ERMAB071")
//    val _: OnePointErrorCode = Value("ERMAB072")
//    val _: OnePointErrorCode = Value("ERMAB073")
//    val _: OnePointErrorCode = Value("ERMAB074")
//    val _: OnePointErrorCode = Value("ERMAB075")
//    val _: OnePointErrorCode = Value("ERMAB076")
//    val _: OnePointErrorCode = Value("ERMAB077")
//    val _: OnePointErrorCode = Value("ERMAB078")
//    val _: OnePointErrorCode = Value("ERMAB079")
//    val _: OnePointErrorCode = Value("ERMAB081")
//    val _: OnePointErrorCode = Value("ERMAB082")
//    val _: OnePointErrorCode = Value("ERMAB083")
//    val _: OnePointErrorCode = Value("ERMAB084")
//    val _: OnePointErrorCode = Value("ERMAB085")
//    val _: OnePointErrorCode = Value("ERMAB086")
//    val _: OnePointErrorCode = Value("ERMAB087")
//    val _: OnePointErrorCode = Value("ERMAB088")
//    val _: OnePointErrorCode = Value("ERMAB089")
//    val _: OnePointErrorCode = Value("ERMAB090")
//    val _: OnePointErrorCode = Value("ERMAB091")
//    val _: OnePointErrorCode = Value("ERMAB092")
//    val _: OnePointErrorCode = Value("ERMAB093")
//    val _: OnePointErrorCode = Value("ERMAB094")
//    val _: OnePointErrorCode = Value("ERMAB095")
//    val _: OnePointErrorCode = Value("ERMAB096")
//    val _: OnePointErrorCode = Value("ERMAB097")
//    val _: OnePointErrorCode = Value("ERMAB098")
//    val _: OnePointErrorCode = Value("ERMAB099")
  }

  object IntelliBestBuy {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERIFS001")
    val InvalidSessionId: OnePointErrorCode = Value("ERIFS002")
//    val _: OnePointErrorCode = Value("ERIFS003")
//    val _: OnePointErrorCode = Value("ERIFS004")
//    val _: OnePointErrorCode = Value("ERIFS005")
//    val _: OnePointErrorCode = Value("ERIFS006")
//    val _: OnePointErrorCode = Value("ERIFS007")
//    val _: OnePointErrorCode = Value("ERIFS008")
//    val _: OnePointErrorCode = Value("ERIFS009")
//    val _: OnePointErrorCode = Value("ERIFS010")
//    val _: OnePointErrorCode = Value("ERIFS011")
//    val _: OnePointErrorCode = Value("ERIFS012")
//    val _: OnePointErrorCode = Value("ERIFS013")
//    val _: OnePointErrorCode = Value("ERIFS014")
//    val _: OnePointErrorCode = Value("ERIFS015")
//    val _: OnePointErrorCode = Value("ERIFS016")
  }

  object OnePointPayment {
    val SessionIdCannotBeNull: OnePointErrorCode = Value("ERPAY001")
    val InvalidSessionId: OnePointErrorCode = Value("ERPAY002")
//    val _: OnePointErrorCode = Value("ERPAY003")
//    val _: OnePointErrorCode = Value("ERPAY004")
//    val _: OnePointErrorCode = Value("ERPAY005")
//    val _: OnePointErrorCode = Value("ERPAY006")
//    val _: OnePointErrorCode = Value("ERPAY007")
//    val _: OnePointErrorCode = Value("ERPAY008")
//    val _: OnePointErrorCode = Value("ERPAY009")
//    val _: OnePointErrorCode = Value("ERPAY010")
  }

  object Emergency {
//    val _: OnePointErrorCode = Value("ERGEN001")
//    val _: OnePointErrorCode = Value("ERGEN002")
//    val _: OnePointErrorCode = Value("ERGEN003")
//    val _: OnePointErrorCode = Value("ERGEN004")
//    val _: OnePointErrorCode = Value("ERGEN005")
//    val _: OnePointErrorCode = Value("ERGEN006")
//    val _: OnePointErrorCode = Value("ERGEN007")
//    val _: OnePointErrorCode = Value("ERGEN008")
//    val _: OnePointErrorCode = Value("ERGEN009")
//    val _: OnePointErrorCode = Value("ERGEN010")
//    val _: OnePointErrorCode = Value("ERGEN011")
//    val _: OnePointErrorCode = Value("ERGEN012")
//    val _: OnePointErrorCode = Value("ERGEN013")
//    val _: OnePointErrorCode = Value("ERGEN014")
//    val _: OnePointErrorCode = Value("ERGEN015")
//    val _: OnePointErrorCode = Value("ERGEN016")
//    val _: OnePointErrorCode = Value("ERGEN017")
//    val _: OnePointErrorCode = Value("ERGEN018")
//    val _: OnePointErrorCode = Value("ERGEN019")
//    val _: OnePointErrorCode = Value("ERMAI001")
  }

}
