package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class CabinBaggageInfo(cabinBaggage: Seq[String])

object CabinBaggageInfo {
  def apply(response: mystifly.CabinBaggageInfo): CabinBaggageInfo =
    CabinBaggageInfo(response.getCabinBaggage.asScala)
}