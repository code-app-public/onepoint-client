package com.codeapp.entities

import onepointentities.onepoint.mystifly

case class Service1_1(extraServiceId: Int)

object Service1_1 {
  def toRequest(factory: mystifly.ObjectFactory)(form: Service1_1): mystifly.Services = {
    val request = factory.createServices()

    request.setExtraServiceId(form.extraServiceId)

    request
  }
}
