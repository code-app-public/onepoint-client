package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class ItinTotalFare(actualFare: Option[Fare]
                         , baseFare: Fare
                         , equivFare: Fare
                         , serviceTax: Option[Fare]
                         , totalFare: Fare
                         , totalTax: Fare)

object ItinTotalFare {
  def apply(response: mystifly.ItinTotalFare): ItinTotalFare =
    ItinTotalFare(
      Option(response.getActualFare).map(data => Fare(data.getValue))
      , Fare(response.getBaseFare.getValue)
      , Fare(response.getEquivFare.getValue)
      , Option(response.getServiceTax).map(data => Fare(data.getValue))
      , Fare(response.getTotalFare.getValue)
      , Fare(response.getTotalTax.getValue)
    )
}