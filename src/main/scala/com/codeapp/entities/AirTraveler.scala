package com.codeapp.entities

import com.codeapp.ObjectFactory
import com.codeapp.entities.Extension.toXMLGregorianCalendar
import com.codeapp.entities.Gender.Gender
import com.codeapp.entities.PassengerType.PassengerType
import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

case class AirTraveler(
                      dateOfBirth: DateTime
                      , extraServices: Option[Seq[Services]] = None
                      , extraServices1_1: Option[Seq[Service1_1]] = None
                      , frequentFlyerNumber: Option[String] = None
                      , gender: Gender
                      , knownTravelerNo: Option[String] = None
                      , nationalID: Option[String] = None
                      , passengerName: PassengerName
                      , passengerNationality: Option[String] = None
                      , passengerType: PassengerType
                      , passport: Option[Passport] = None
                      , redressNo: Option[String] = None
                      , residentFareDocumentType: Option[ResidentFareDocumentType] = None
                      , specialServiceRequest: Option[SpecialServiceRequest] = None
                      )

object AirTraveler {
  private val onePointMystiflyFactory = ObjectFactory.onePointEntityMystifly

  def toRequest(factory: mystifly.ObjectFactory)(form: AirTraveler): mystifly.AirTraveler = {
    val request = factory.createAirTraveler()

    request.setDateOfBirth(factory.createAirTravelerDateOfBirth(toXMLGregorianCalendar(form.dateOfBirth)))

    form.extraServices.foreach { ex =>
      val arrayOfExtraService = factory.createArrayOfServices()
      ex.map(Services.toRequest(factory)).foreach(arrayOfExtraService.getServices.add)
      request.setExtraServices(factory.createAirTravelerExtraServices(arrayOfExtraService))
    }

    form.extraServices1_1.foreach { ex =>
      val arrayOfExtraService1_1 = onePointMystiflyFactory.createArrayOfServices()
      ex.map(Service1_1.toRequest(onePointMystiflyFactory)).foreach(arrayOfExtraService1_1.getServices.add)
      request.setExtraServices11(factory.createAirTravelerExtraServices11(arrayOfExtraService1_1))
    }

    form.frequentFlyerNumber.map(factory.createAirTravelerFrequentFlyerNumber).foreach(request.setFrequentFlyerNumber)
    request.setGender(mystifly.Gender.fromValue(form.gender.toString))
    form.knownTravelerNo.map(factory.createAirTravelerKnowTravelerNo).foreach(request.setKnowTravelerNo)
    form.nationalID.map(factory.createAirTravelerNationalID).foreach(request.setNationalID)
    request.setPassengerName(factory.createAirTravelerPassengerName(PassengerName.toRequest(factory, form.passengerName)))
    form.passengerNationality.map(factory.createAirTravelerPassengerNationality).foreach(request.setPassengerNationality)
    request.setPassengerType(mystifly.PassengerType.fromValue(form.passengerType.toString))
    form.passport.map(Passport.toRequest(factory)).map(factory.createAirTravelerPassport).foreach(request.setPassport)
    form.redressNo.map(factory.createAirTravelerRedressNo).foreach(request.setRedressNo)
    form.residentFareDocumentType.map(ResidentFareDocumentType.toRequest(factory))
      .map(factory.createAirTravelerResidentFareDocumentType)
      .foreach(request.setResidentFareDocumentType)
    form.specialServiceRequest.map(SpecialServiceRequest.toRequest(factory))
      .map(factory.createAirTravelerSpecialServiceRequest)
      .foreach(request.setSpecialServiceRequest)

    request
  }
}
