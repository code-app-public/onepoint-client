package com.codeapp.entities

import com.codeapp.entities.AirTripType.AirTripType
import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class PricedItinerary(airItineraryPricingInfo: AirItineraryPricingInfo
                           , directionInd: AirTripType
                           , isPassportMandatory: Boolean
                           , isResidentFare: Boolean
                           , originDestinationOptions: Seq[OriginDestinationOption]
                           , paxNameCharacterLimit: Int
                           , requiredFieldsToBook: Seq[String]
                           , residentFareDocumentTypes: Seq[String]
                           , sequenceNumber: Int
                           , ticketType: String
                           , validatingAirlineCode: String
                          )

object PricedItinerary {
  def apply(response: mystifly.PricedItinerary): PricedItinerary =
    PricedItinerary(
      AirItineraryPricingInfo(response.getAirItineraryPricingInfo.getValue)
      , AirTripType.withName(response.getDirectionInd.value())
      , response.isIsPassportMandatory
      , response.isIsResidentFare
      , response.getOriginDestinationOptions.getValue.getOriginDestinationOption.asScala.map(OriginDestinationOption.apply)
      , response.getPaxNameCharacterLimit
      , Seq()
      , Seq()
      , response.getSequenceNumber
      , response.getTicketType.getValue
      , response.getValidatingAirlineCode.getValue
    )
}

