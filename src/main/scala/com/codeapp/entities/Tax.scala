package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class Tax(amount: String, currencyCode: String, decimalPlaces: Int, taxCode: String)

object Tax {
  def apply(response: mystifly.Tax): Tax =
    Tax(
      response.getAmount.getValue
      , response.getCurrencyCode.getValue
      , response.getDecimalPlaces
      , response.getTaxCode.getValue
    )
}