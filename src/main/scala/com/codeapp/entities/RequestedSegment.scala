package com.codeapp.entities

import com.codeapp.entities.Extension.toXMLGregorianCalendar
import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

case class RequestedSegment(departureDateTime: Option[DateTime] = None
                            , destination: Option[String] = None
                            , flightNumber: Option[String] = None
                            , origin: Option[String] = None
                            , requestSSRs: Option[Seq[RequestSSR]] = None
                            , seatSelection: Option[String] = None)

object RequestedSegment {
  def toRequest(factory: mystifly.ObjectFactory)(form: RequestedSegment): mystifly.RequestedSegment = {
    val request = factory.createRequestedSegment()

    form.departureDateTime.map(toXMLGregorianCalendar).foreach(request.setDepartureDateTime)
    form.destination.map(factory.createRequestedSegmentDestination).foreach(request.setDestination)
    form.flightNumber.map(factory.createRequestedSegmentFlightNumber).foreach(request.setFlightNumber)
    form.origin.map(factory.createRequestedSegmentOrigin).foreach(request.setOrigin)

    form.requestSSRs.foreach { ssrs =>
      val array = factory.createArrayOfRequestSSR()
      ssrs
        .map(RequestSSR.toRequest(factory, _))
        .foreach(array.getRequestSSR.add)
      request.setRequestSSRs(factory.createRequestedSegmentRequestSSRs(array))
    }

    form.seatSelection.map(factory.createRequestedSegmentSeatSelection).foreach(request.setSeatSelection)

    request
  }
}

