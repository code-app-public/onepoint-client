package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object MealPreference extends Enumeration {
  type MealPreference = Value

  val ANY: MealPreference = Value(mystifly.MealPreference.ANY.value())
  val AVML: MealPreference = Value(mystifly.MealPreference.AVML.value())
  val BBML: MealPreference = Value(mystifly.MealPreference.BBML.value())
  val BLML: MealPreference = Value(mystifly.MealPreference.BLML.value())
  val CHML: MealPreference = Value(mystifly.MealPreference.CHML.value())
  val DBML: MealPreference = Value(mystifly.MealPreference.DBML.value())
  val FFML: MealPreference = Value(mystifly.MealPreference.FFML.value())
  val FPML: MealPreference = Value(mystifly.MealPreference.FPML.value())
  val GFML: MealPreference = Value(mystifly.MealPreference.GFML.value())
  val HFML: MealPreference = Value(mystifly.MealPreference.HFML.value())
  val HNML: MealPreference = Value(mystifly.MealPreference.HNML.value())
  val JNML: MealPreference = Value(mystifly.MealPreference.JNML.value())
  val KSML: MealPreference = Value(mystifly.MealPreference.KSML.value())
  val LCML: MealPreference = Value(mystifly.MealPreference.LCML.value())
  val LFML: MealPreference = Value(mystifly.MealPreference.LFML.value())
  val LPML: MealPreference = Value(mystifly.MealPreference.LPML.value())
  val LSML: MealPreference = Value(mystifly.MealPreference.LSML.value())
  val MOML: MealPreference = Value(mystifly.MealPreference.MOML.value())
  val NLML: MealPreference = Value(mystifly.MealPreference.NLML.value())
  val NSML: MealPreference = Value(mystifly.MealPreference.NSML.value())
  val ORML: MealPreference = Value(mystifly.MealPreference.ORML.value())
  val PFML: MealPreference = Value(mystifly.MealPreference.PFML.value())
  val PRML: MealPreference = Value(mystifly.MealPreference.PRML.value())
  val RVML: MealPreference = Value(mystifly.MealPreference.RVML.value())
  val SFML: MealPreference = Value(mystifly.MealPreference.SFML.value())
  val SPMLJ: MealPreference = Value(mystifly.MealPreference.SPMLJ.value())
  val VGML: MealPreference = Value(mystifly.MealPreference.VGML.value())
  val VJML: MealPreference = Value(mystifly.MealPreference.VJML.value())
  val VLML: MealPreference = Value(mystifly.MealPreference.VLML.value())
  val VOML: MealPreference = Value(mystifly.MealPreference.VOML.value())
  val VVML: MealPreference = Value(mystifly.MealPreference.VVML.value())
}

