package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class Error(code: String, message: String)

object Error {
  def apply(error: mystifly.Error): Error =
    Error(error.getCode.getValue, error.getMessage.getValue)
}