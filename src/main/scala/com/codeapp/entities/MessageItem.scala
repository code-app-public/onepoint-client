package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

import scala.collection.JavaConverters._

case class MessageItem(bookingMode: String
                       , messages: Seq[String]
                       , RPH: Int
                       , scheduleChangeIdentifiedAt: Option[DateTime]
                       , tktTimeLimit: DateTime
                       , travelStartDate: Option[DateTime]
                       , typeOfScheduleChange: String
                       , uniqueId: String)

object MessageItem {
  def apply(response: mystifly.MessageItem): MessageItem =
    MessageItem(
      response.getBookingMode.getValue
      , response.getMessages.getValue.getString.asScala
      , response.getRPH
      , Option(response.getScheduleChangeIdentifiedAt).flatMap(d => Option(d.getValue)).map(_.toString).map(DateTime.parse)
      , DateTime.parse(response.getTktTimeLimit.getValue.toString)
      , Option(response.getTravelStartDate).flatMap(d => Option(d.getValue)).map(_.toString).map(DateTime.parse
      )
      , response.getTypeOfScheduleChange.getValue
      , response.getUniqueID.getValue
    )
}
