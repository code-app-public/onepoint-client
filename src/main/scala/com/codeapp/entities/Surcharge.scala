package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class Surcharge(amount: Double, indicator: String, `type`: String)

object Surcharge {
  def apply(response: mystifly.Surcharge): Surcharge =
    Surcharge(response.getAmount, response.getIndicator.getValue, response.getType.getValue)
}