package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

import scala.collection.JavaConverters._


case class ItineraryInfo(bookedReservedItems: Seq[ReservationItem]
                         , clientUTCOffset: Double
                         , customerInfos: Seq[CustomerInfo]
                         , itineraryPricing: ItineraryPricing
                         , reservationItems: Seq[ReservationItem]
                         , tripDetailPTCFareBreakdown: Seq[TripDetailsPTCFareBreakdown]
                         , voidingWindow: Option[DateTime])

object ItineraryInfo {
  def apply(response: mystifly.ItineraryInfo): ItineraryInfo =
    ItineraryInfo(
      response.getBookedReservedItems.getValue.getReservationItem.asScala.map(ReservationItem.apply)
      , response.getClientUTCOffset
      , response.getCustomerInfos.getValue.getCustomerInfo.asScala.map(CustomerInfo.apply)
      , ItineraryPricing(response.getItineraryPricing.getValue)
      , response.getReservationItems.getValue.getReservationItem.asScala.map(ReservationItem.apply)
      , response.getTripDetailsPTCFareBreakdowns.getValue.getTripDetailsPTCFareBreakdown.asScala.map(TripDetailsPTCFareBreakdown.apply)
      , Option(response.getVoidingWindow).flatMap(d => Option(d.getValue)).map(_.toString).map(DateTime.parse)
    )
}
