package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class ServiceCost(amount: String, currencyCode: String, decimalPlaces: Int)

object ServiceCost {
  def apply(response: mystifly.ServiceCost): ServiceCost =
    ServiceCost(response.getAmount.getValue, response.getCurrencyCode.getValue, response.getDecimalPlaces)
}