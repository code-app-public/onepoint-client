package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object PenaltyType extends Enumeration {
  type PenaltyType = Value

  val EXCHANGE: PenaltyType = Value(mystifly.PenaltyType.EXCHANGE.value())
  val CANCEL: PenaltyType = Value(mystifly.PenaltyType.CANCEL.value())
}
