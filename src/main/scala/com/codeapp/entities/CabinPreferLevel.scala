package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object CabinPreferLevel extends Enumeration {
  type CabinPreferLevel = Value

  val DEFAULT: CabinPreferLevel = Value(mystifly.CabinPreferLevel.DEFAULT.value())
  val RESTRICTED: CabinPreferLevel = Value(mystifly.CabinPreferLevel.RESTRICTED.value())
  val PREFERRED: CabinPreferLevel = Value(mystifly.CabinPreferLevel.PREFERRED.value())
}
