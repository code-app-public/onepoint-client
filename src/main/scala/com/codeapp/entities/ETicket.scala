package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class ETicket(number: String, itemPRH: Int)

object ETicket {
  def apply(response: mystifly.ETicket): ETicket =
    ETicket(response.getETicketNumber.getValue, response.getItemRPH)
}