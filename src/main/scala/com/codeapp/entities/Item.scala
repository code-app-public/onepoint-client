package com.codeapp.entities

import com.codeapp.entities.QueueCategory.QueueCategory
import org.datacontract.schemas._2004._07.mystifly

case class Item(categoryId: QueueCategory, uniqueId: String)

object Item {
  def toRequest(factory: mystifly.ObjectFactory)(form: Item): mystifly.Item = {
    val item = factory.createItem()

    item.setCategoryId(mystifly.QueueCategory.fromValue(form.categoryId.toString))
    item.setUniqueId(factory.createItemUniqueId(form.uniqueId))

    item
  }
}
