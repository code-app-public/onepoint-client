package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

import scala.collection.JavaConverters._

case class FlightSegment(arrivalAirportLocationCode: String
                         , arrivalDateTime: DateTime
                         , cabinClassCode: String
                         , cabinClassText: String
                         , departureAirportLocationCode: String
                         , departureDateTime: DateTime
                         , eTicket: Boolean
                         , flightNumber: String
                         , journeyDuration: Int
                         , marketingAirlineCode: String
                         , marriageGroup: String
                         , mealCode: String
                         , operatingAirline: OperatingAirline
                         , resBookDesigCode: String
                         , resBookDesigText: String
                         , seatsRemaining: SeatsRemaining
                         , stopQuantity: Int
                         , stopQuantityInfo: StopQuantityInfo
                         , stopQuantityInformations: Option[Seq[StopQuantityInfo]])

object FlightSegment {
  def apply(response: mystifly.FlightSegment): FlightSegment =
    FlightSegment(
      response.getArrivalAirportLocationCode.getValue
      , DateTime.parse(response.getArrivalDateTime.toString)
      , response.getCabinClassCode.getValue
      , response.getCabinClassText.getValue
      , response.getDepartureAirportLocationCode.getValue
      , DateTime.parse(response.getDepartureDateTime.toString)
      , response.isEticket
      , response.getFlightNumber.getValue
      , response.getJourneyDuration
      , response.getMarketingAirlineCode.getValue
      , response.getMarriageGroup.getValue
      , response.getMealCode.getValue
      , OperatingAirline(response.getOperatingAirline.getValue)
      , response.getResBookDesigCode.getValue
      , response.getResBookDesigText.getValue
      , SeatsRemaining(response.getSeatsRemaining.getValue)
      , response.getStopQuantity
      , StopQuantityInfo(response.getStopQuantityInfo.getValue)
      , Option(response.getStopQuantityInformations).map(_.getValue.getStopQuantityInfo.asScala.map(StopQuantityInfo.apply))
    )
}
