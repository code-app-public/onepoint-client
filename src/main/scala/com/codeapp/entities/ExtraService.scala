package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class ExtraService(services: Seq[Service])

object ExtraService {
  def apply(response: mystifly.ExtraService): ExtraService =
    ExtraService(response.getServices.getValue.getService.asScala.map(Service.apply))
}
