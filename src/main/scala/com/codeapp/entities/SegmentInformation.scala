package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly
import scala.collection.JavaConverters._

case class SegmentInformation(destination: String
                              , flightNumber: String
                              , origin: String
                              , requestedSSRs: Seq[RequestedSSR]
                              , seatSelection: String
                              , seatSelectionStatus: String)

object SegmentInformation {
  def apply(response: mystifly.SegmentInformation): SegmentInformation =
    SegmentInformation(
      response.getDestination.getValue
      , response.getFlightNumber.getValue
      , response.getOrigin.getValue
      , response.getRequestedSSRs.getValue.getRequestedSSR.asScala.map(RequestedSSR.apply)
      , response.getSeatSelection.getValue
      , response.getSeatSelectionStatus.getValue
    )
}