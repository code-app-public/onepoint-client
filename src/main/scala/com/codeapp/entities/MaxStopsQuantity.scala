package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object MaxStopsQuantity extends Enumeration {
  type MaxStopsQuantity = Value

  val DIRECT: MaxStopsQuantity = Value(mystifly.MaxStopsQuantity.DIRECT.value())
  val ONE_STOP: MaxStopsQuantity = Value(mystifly.MaxStopsQuantity.ONE_STOP.value())
  val ALL: MaxStopsQuantity = Value(mystifly.MaxStopsQuantity.ALL.value())
}
