package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object PassengerTitle extends Enumeration {
  type PassengerTitle = Value

  val DEFAULT: PassengerTitle = Value(mystifly.PassengerTitle.DEFAULT.value())
  val INF: PassengerTitle = Value(mystifly.PassengerTitle.INF.value())
  val LADY: PassengerTitle = Value(mystifly.PassengerTitle.LADY.value())
  val LORD: PassengerTitle = Value(mystifly.PassengerTitle.LORD.value())
  val MISS: PassengerTitle = Value(mystifly.PassengerTitle.MISS.value())
  val MR: PassengerTitle = Value(mystifly.PassengerTitle.MR.value())
  val MRS: PassengerTitle = Value(mystifly.PassengerTitle.MRS.value())
  val MS: PassengerTitle = Value(mystifly.PassengerTitle.MS.value())
  val MSTR: PassengerTitle = Value(mystifly.PassengerTitle.MSTR.value())
  val SIR: PassengerTitle = Value(mystifly.PassengerTitle.SIR.value())
}
