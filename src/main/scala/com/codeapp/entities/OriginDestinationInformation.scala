package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly
import org.joda.time.DateTime

case class OriginDestinationInformation(departureDateTime: DateTime
                                        , destinationLocationCode: String
                                        , originLocationCode: String
                                        , arrivalWindow: Option[String] = None
                                        , departureWindow: Option[String] = None)

object OriginDestinationInformation {
  def toRequest(factory: mystifly.ObjectFactory)(form: OriginDestinationInformation): mystifly.OriginDestinationInformation = {
    val originDestinationInformation = factory.createOriginDestinationInformation()

    originDestinationInformation.setOriginLocationCode(factory.createOriginDestinationInformationOriginLocationCode(form.originLocationCode))
    originDestinationInformation.setDestinationLocationCode(factory.createOriginDestinationInformationDestinationLocationCode(form.destinationLocationCode))
    originDestinationInformation.setDepartureDateTime(Extension.toXMLGregorianCalendar(form.departureDateTime))

    form.arrivalWindow.map(factory.createOriginDestinationInformationArrivalWindow).foreach(originDestinationInformation.setArrivalWindow)
    form.departureWindow.map(factory.createOriginDestinationInformationDepartureWindow).foreach(originDestinationInformation.setDepartureWindow)

    originDestinationInformation
  }
}