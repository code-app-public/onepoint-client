package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly
import com.codeapp.entities.SSRCode.SSRCode

case class RequestedSSR(code: SSRCode, status: String)

object RequestedSSR {
  def apply(response: mystifly.RequestedSSR): RequestedSSR =
    RequestedSSR(
      SSRCode.withName(response.getSSRCode.value())
      , response.getStatus.getValue
    )
}