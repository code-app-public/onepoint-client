package com.codeapp.entities

import java.util.{Date, GregorianCalendar, TimeZone}

import javax.xml.bind.JAXBElement
import javax.xml.datatype.{DatatypeFactory, XMLGregorianCalendar}
import org.datacontract.schemas._2004._07.mystifly.ArrayOfError
import org.joda.time.DateTime

import scala.collection.JavaConverters._

object Extension {
  def toXMLGregorianCalendar(dateTime: DateTime): XMLGregorianCalendar = {
    val date = new Date(dateTime.getMillis)
    val gregorianCalendar = new GregorianCalendar()
    gregorianCalendar.setTimeZone(TimeZone.getTimeZone("+00:00"))
    gregorianCalendar.setTime(date)

    DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar)
  }

  def validateResponse[That](isSuccess: Boolean
                             , errorsXML: JAXBElement[ArrayOfError])
                            (result: That): That = {
    val errors = errorsXML.getValue.getError.asScala.map(Error.apply)

    if (errors.nonEmpty && !isSuccess) {
      val error = errors.head
      throw new OnePointException(error.code, error.message)
    } else if (!isSuccess) throw new Exception()
    else result
  }
}
