package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object AirTripType extends Enumeration {
  type AirTripType = Value

  val ONE_WAY: AirTripType = Value(mystifly.AirTripType.ONE_WAY.value())
  val RETURN: AirTripType = Value(mystifly.AirTripType.RETURN.value())
  val OPEN_JAW: AirTripType = Value(mystifly.AirTripType.OPEN_JAW.value())
  val CIRCLE: AirTripType = Value(mystifly.AirTripType.CIRCLE.value())
}

