package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class ItineraryPricing(clientMarkup: Option[Fare]
                            , equiFare: Fare
                            , extraServiceCharge: Option[Fare]
                            , serviceTax: Option[Fare]
                            , tax: Fare
                            , totalFare: Fare)

object ItineraryPricing {
  def apply(response: mystifly.ItineraryPricing): ItineraryPricing =
    ItineraryPricing(
      Option(response.getClientMarkup).map(_.getValue).map(Fare.apply)
      , Fare(response.getEquiFare.getValue)
      , Option(response.getExtraServiceCharges).map(_.getValue).map(Fare.apply)
      , Option(response.getServiceTax).map(_.getValue).map(Fare.apply)
      , Fare(response.getTax.getValue)
      , Fare(response.getTotalFare.getValue)
    )
}

