package com.codeapp.entities

import com.codeapp.entities.PassengerTitle.PassengerTitle
import org.datacontract.schemas._2004._07.mystifly

case class PassengerName(firstName: String, lastName: String, title: PassengerTitle)

object PassengerName {
  def toRequest(factory: mystifly.ObjectFactory, form: PassengerName): mystifly.PassengerName = {
    val request = factory.createPassengerName()

    request.setPassengerFirstName(factory.createPassengerNamePassengerFirstName(form.firstName))
    request.setPassengerLastName(factory.createPassengerNamePassengerLastName(form.lastName))
    request.setPassengerTitle(mystifly.PassengerTitle.fromValue(form.title.toString))

    request
  }
}