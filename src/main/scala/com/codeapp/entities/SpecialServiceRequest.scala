package com.codeapp.entities

import com.codeapp.entities.MealPreference.MealPreference
import com.codeapp.entities.SeatPreference.SeatPreference
import org.datacontract.schemas._2004._07.mystifly

case class SpecialServiceRequest(mealPreference: Option[MealPreference] = None
                                 , requestedSegments: Option[Seq[RequestedSegment]] = None
                                 , seatPreference: Option[SeatPreference] = None)

object SpecialServiceRequest {
  def toRequest(factory: mystifly.ObjectFactory)(form: SpecialServiceRequest): mystifly.SpecialServiceRequest = {
    val request = factory.createSpecialServiceRequest()

    form.mealPreference.map(_.toString)
      .map(mystifly.MealPreference.fromValue)
      .foreach(request.setMealPreference)

    form.requestedSegments.foreach { requestSegments =>
      val array = factory.createArrayOfRequestedSegment()

      requestSegments
        .map(RequestedSegment.toRequest(factory))
        .foreach(array.getRequestedSegment.add)

      array
    }
    form.seatPreference.map(_.toString)
      .map(mystifly.SeatPreference.fromValue)
      .foreach(request.setSeatPreference)

    request
  }
}


