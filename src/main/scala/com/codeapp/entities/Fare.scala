package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class Fare(amount: String, currencyCode: String, decimalPlaces: Int)

object Fare {
  def apply(response: mystifly.Fare): Fare =
    Fare(response.getAmount.getValue, response.getCurrencyCode.getValue, response.getDecimalPlaces)
}