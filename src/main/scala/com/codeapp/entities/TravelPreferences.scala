package com.codeapp.entities

import com.codeapp.entities.AirTripType.AirTripType
import com.codeapp.entities.MaxStopsQuantity.MaxStopsQuantity
import org.datacontract.schemas._2004._07.mystifly

case class TravelPreferences(airTripType: AirTripType
                             , cabinPreference: CabinPreference
                             , maxStopsQuantity: MaxStopsQuantity
                             , vendorExcludeCodes: Option[Seq[String]] = None
                             , vendorPreferenceCodes: Option[Seq[String]] = None)

object TravelPreferences {
  def toRequest(factory: mystifly.ObjectFactory, form: TravelPreferences): mystifly.TravelPreferences = {
    val travelPreferences = factory.createTravelPreferences()

    travelPreferences.setAirTripType(mystifly.AirTripType.fromValue(form.airTripType.toString))
    travelPreferences.setCabinPreference(mystifly.CabinType.fromValue(form.cabinPreference.`type`.toString))
    travelPreferences.setMaxStopsQuantity(mystifly.MaxStopsQuantity.fromValue(form.maxStopsQuantity.toString))

    form.cabinPreference.level
      .map { _ =>
        val cabinClassPreference = factory.createCabinClassPreference()
        cabinClassPreference.setCabinType(mystifly.CabinType.fromValue(form.cabinPreference.`type`.toString))
        form.cabinPreference.level
          .map(_.toString)
          .map(mystifly.CabinPreferLevel.fromValue)
          .foreach(cabinClassPreference.setPreferenceLevel)

        val preferences = factory.createPreferences()
        preferences.setCabinClassPreference(factory.createCabinClassPreference(cabinClassPreference))

        preferences
      }
      .map(factory.createTravelPreferencesPreferences)
      .foreach(travelPreferences.setPreferences)

    travelPreferences
  }
}