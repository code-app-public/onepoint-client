package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object Eligibility extends Enumeration {
  type Eligibility = Value

  val DEFAULT: Eligibility = Value(mystifly.Eligibility.DEFAULT.value())
  val ES_1: Eligibility = Value(mystifly.Eligibility.ES_1.value())
  val ES_2: Eligibility = Value(mystifly.Eligibility.ES_2.value())
  val ES_3: Eligibility = Value(mystifly.Eligibility.ES_3.value())
  val ES_4: Eligibility = Value(mystifly.Eligibility.ES_4.value())
  val ES_5: Eligibility = Value(mystifly.Eligibility.ES_5.value())
  val ES_6: Eligibility = Value(mystifly.Eligibility.ES_6.value())
  val ES_7: Eligibility = Value(mystifly.Eligibility.ES_7.value())
  val ES_8: Eligibility = Value(mystifly.Eligibility.ES_8.value())
}
