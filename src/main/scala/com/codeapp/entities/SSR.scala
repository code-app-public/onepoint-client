package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class SSR(itemRPH: Int
               , mealPreference: String
               , seatPreference: String
               , segmentInformations: Seq[SegmentInformation])

object SSR {
  def apply(response: mystifly.SSR): SSR =
    SSR(
      response.getItemRPH
      , response.getMealPreference.getValue
      , response.getSeatPreference.getValue
      , response.getSegmentInformations.getValue.getSegmentInformation.asScala.map(SegmentInformation.apply)
    )
}