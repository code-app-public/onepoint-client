package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class OperatingAirline(code: String, equipment: String, flightNumber: String)

object OperatingAirline {
  def apply(response: mystifly.OperatingAirline): OperatingAirline =
    OperatingAirline(
      response.getCode.getValue
      , response.getEquipment.getValue
      , response.getFlightNumber.getValue
    )
}

