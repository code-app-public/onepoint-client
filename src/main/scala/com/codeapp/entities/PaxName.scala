package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class PaxName(firstName: String, lastName: String, title: String)

object PaxName {
  def apply(response: mystifly.PaxName): PaxName =
    PaxName(response.getPassengerFirstName.getValue, response.getPassengerLastName.getValue, response.getPassengerTitle.getValue)
}