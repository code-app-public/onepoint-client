package com.codeapp.entities

import com.codeapp.entities.CabinPreferLevel.CabinPreferLevel
import com.codeapp.entities.CabinType.CabinType

case class CabinPreference(`type`: CabinType, level: Option[CabinPreferLevel])
