package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class TravelerInfo(
                         airTravelers: Seq[AirTraveler]
                         , areaCode: String
                         , countryCode: String
                         , email: String
                         , phoneNumber: String
                         , postCode: Option[String] = None
                       )

object TravelerInfo {
  def toRequest(factory: mystifly.ObjectFactory, form: TravelerInfo): mystifly.TravelerInfo = {
    val request = factory.createTravelerInfo()

    val array = factory.createArrayOfAirTraveler()
    form.airTravelers
      .map(AirTraveler.toRequest(factory))
      .foreach(array.getAirTraveler.add)
    request.setAirTravelers(factory.createTravelerInfoAirTravelers(array))

    request.setAreaCode(factory.createTravelerInfoAreaCode(form.areaCode))
    request.setCountryCode(factory.createTravelerInfoCountryCode(form.countryCode))
    request.setEmail(factory.createTravelerInfoEmail(form.email))
    request.setPhoneNumber(factory.createTravelerInfoPhoneNumber(form.phoneNumber))
    form.postCode.map(factory.createTravelerInfoPostCode).foreach(request.setPostCode)

    request
  }
}
