package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class CustomerInfo(customer: Customer, eTickets: Seq[ETicket], SSRs: Seq[SSR])

object CustomerInfo {
  def apply(response: mystifly.CustomerInfo): CustomerInfo =
    CustomerInfo(
      Customer(response.getCustomer.getValue)
      , response.getETickets.getValue.getETicket.asScala.map(ETicket.apply)
      , response.getSSRs.getValue.getSSR.asScala.map(SSR.apply)
    )
}