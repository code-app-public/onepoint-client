package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object SeatPreference extends Enumeration {
  type SeatPreference = Value

  val ANY: SeatPreference = Value(mystifly.SeatPreference.ANY.value())
  val A: SeatPreference = Value(mystifly.SeatPreference.A.value())
  val W: SeatPreference = Value(mystifly.SeatPreference.W.value())
}

