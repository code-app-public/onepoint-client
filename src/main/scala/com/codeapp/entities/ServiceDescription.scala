package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly
import com.codeapp.entities.Eligibility.Eligibility

case class ServiceDescription(description: String
                              , eligibility: Eligibility
                              , isGroup: Boolean
                              , maxQuantity: String)

object ServiceDescription {
  def apply(response: mystifly.ServiceDescription): ServiceDescription =
    ServiceDescription(
      response.getDescription.getValue
      , Eligibility.withName(response.getEligibility.value())
      , response.isIsGroup
      , response.getMaxQuantity.getValue
    )
}