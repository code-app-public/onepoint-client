package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

case class Services(extraServiceId: Int, quantity: Int)

object Services {
  def toRequest(factory: mystifly.ObjectFactory)(form: Services): mystifly.Services = {
    val request = factory.createServices()

    request.setExtraServiceId(form.extraServiceId)
    request.setQuantity(form.quantity)

    request
  }
}
