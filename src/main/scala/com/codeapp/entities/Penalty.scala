package com.codeapp.entities

import com.codeapp.entities.PenaltyType.PenaltyType
import org.datacontract.schemas._2004._07.mystifly

case class Penalty(amount: String, currencyCode: String, `type`: PenaltyType)

object Penalty {
  def apply(response: mystifly.Penalty): Penalty =
    Penalty(
      response.getAmount.getValue
      , response.getCurrencyCode.getValue
      , PenaltyType.withName(response.getPenaltyType.value())
    )
}

