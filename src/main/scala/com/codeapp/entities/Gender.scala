package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object Gender extends Enumeration {
  type Gender = Value

  val DEFAULT: Gender = Value(mystifly.Gender.DEFAULT.value())
  val F: Gender = Value(mystifly.Gender.F.value())
  val M: Gender = Value(mystifly.Gender.M.value())
  val U: Gender = Value(mystifly.Gender.U.value())
}
