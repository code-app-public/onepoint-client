package com.codeapp.entities

import com.codeapp.entities.FareType.FareType
import org.datacontract.schemas._2004._07.mystifly

import scala.collection.JavaConverters._

case class AirItineraryPricingInfo(crossBorderIndicator: Option[String]
                                   , divideInPartyIndicator: Boolean
                                   , fareInfo: Seq[FareInfo]
                                   , fareSourceCode: String
                                   , fareType: FareType
                                   , isCommissionable: Option[String]
                                   , isMOFare: Option[String]
                                   , isRefundable: Boolean
                                   , itinTotalFare: ItinTotalFare
                                   , ptcFareBreakdowns: Seq[PTCFareBreakdown]
                                   , splitItinerary: Option[Boolean]
                                  )

object AirItineraryPricingInfo {

  def apply(response: mystifly.AirItineraryPricingInfo): AirItineraryPricingInfo =
    AirItineraryPricingInfo(
      Option(response.getCrossBorderIndicator).map(_.getValue)
      , response.isDivideInPartyIndicator
      , response.getFareInfos.getValue.getFareInfo.asScala.map(FareInfo.apply)
      , response.getFareSourceCode.getValue
      , FareType.withName(response.getFareType.value())
      , Option(response.getIsCommissionable).map(_.getValue)
      , Option(response.getIsMOFare).map(_.getValue)
      , response.getIsRefundable match {
        case mystifly.IsRefundable.YES => true
        case _ => false
      }
      , ItinTotalFare(response.getItinTotalFare.getValue)
      , response.getPTCFareBreakdowns.getValue.getPTCFareBreakdown.asScala.map(PTCFareBreakdown.apply)
      , Option(response.getSplitItinerary).map(_.getValue match {
        case "false" => false
        case _ => true
      })
    )

}

