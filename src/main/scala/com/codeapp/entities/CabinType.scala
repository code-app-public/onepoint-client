package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object CabinType extends Enumeration {
  type CabinType = Value

  val Y: CabinType = Value(mystifly.CabinType.Y.value())
  val S: CabinType = Value(mystifly.CabinType.S.value())
  val C: CabinType = Value(mystifly.CabinType.C.value())
  val F: CabinType = Value(mystifly.CabinType.F.value())
}
