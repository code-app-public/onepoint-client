package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object QueueCategory extends Enumeration {
  type QueueCategory = Value

  val API_URGENT: QueueCategory = Value(mystifly.QueueCategory.API_URGENT.value())
  val BOOKING: QueueCategory = Value(mystifly.QueueCategory.BOOKING.value())
  val CANCELLED: QueueCategory = Value(mystifly.QueueCategory.CANCELLED.value())
  val HX: QueueCategory = Value(mystifly.QueueCategory.HX.value())
  val REFUND_QUOTE: QueueCategory = Value(mystifly.QueueCategory.REFUND_QUOTE.value())
  val REFUNDED: QueueCategory = Value(mystifly.QueueCategory.REFUNDED.value())
  val SCHEDULE_CHANGE: QueueCategory = Value(mystifly.QueueCategory.SCHEDULE_CHANGE.value())
  val TICKETED: QueueCategory = Value(mystifly.QueueCategory.TICKETED.value())
  val TKT_TIME_LIMIT_CHANGE: QueueCategory = Value(mystifly.QueueCategory.TKT_TIME_LIMIT_CHANGE.value())
  val URGENT: QueueCategory = Value(mystifly.QueueCategory.URGENT.value())
  val VOIDED: QueueCategory = Value(mystifly.QueueCategory.VOIDED.value())
}
