package com.codeapp.entities

import org.datacontract.schemas._2004._07.mystifly

object RequestOptions extends Enumeration {
  type RequestOptions = Value

  val FIFTY: RequestOptions = Value(mystifly.RequestOptions.FIFTY.value())
  val HUNDRED: RequestOptions = Value(mystifly.RequestOptions.HUNDRED.value())
  val TWO_HUNDRED: RequestOptions = Value(mystifly.RequestOptions.TWO_HUNDRED.value())
}
