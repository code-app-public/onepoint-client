package com.codeapp

import javax.inject.{Inject, Singleton}
import onepoint.mystifly.OnePoint_Service
import forms._
import result._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class OnePointClient @Inject()(onePointService: OnePoint_Service) {

  private val client = onePointService.basicHttpBindingIOnePoint

  def createSession(form: SessionCreateForm)
                   (implicit ec: ExecutionContext): Future[SessionCreateResult] =
    client
      .createSession(SessionCreateForm.toRequest(form))
      .map(SessionCreateResult.apply)

  def airLowFareSearch(form: AirLowFareSearchForm)
                      (implicit ec: ExecutionContext): Future[AirLowFareSearchResult] =
    client
      .airLowFareSearch(AirLowFareSearchForm.toRequest(form))
      .map(AirLowFareSearchResult.apply)

  def bookFlight(form: AirBookForm)
                (implicit ec: ExecutionContext): Future[AirBookResult] =
    client
      .bookFlight(AirBookForm.toRequest(form))
      .map(AirBookResult.apply)

  def airRevalidate(form: AirRevalidateForm)
                   (implicit ec: ExecutionContext): Future[AirRevalidateResult] =
    client
      .airRevalidate(AirRevalidateForm.toRequest(form))
      .map(AirRevalidateResult.apply)

  def ticketOrder(form: AirOrderTicketForm)
                 (implicit ec: ExecutionContext): Future[AirOrderTicketResult] =
    client
      .ticketOrder(AirOrderTicketForm.toRequest(form))
      .map(AirOrderTicketResult.apply)

  def tripDetails(form: AirTripDetailForm)
                 (implicit ec: ExecutionContext): Future[AirTripDetailResult] =
    client
      .tripDetails(AirTripDetailForm.toRequest(form))
      .map(AirTripDetailResult.apply)

  def messageQueues(form: AirMessageQueueForm)
                   (implicit ec: ExecutionContext): Future[AirMessageQueueResult] =
    client
      .messageQueues(AirMessageQueueForm.toRequest(form))
      .map(AirMessageQueueResult.apply)

  def removeMessageQueues(form: AirRemoveMessageQueueForm)
                         (implicit ec: ExecutionContext): Future[AirRemoveMessageQueueResult] =
    client
      .removeMessageQueues(AirRemoveMessageQueueForm.toRequest(form))
      .map(AirRemoveMessageQueueResult.apply)

  def fareRules11(form: FareRules11Form)
                 (implicit ec: ExecutionContext): Future[FareRules11Result] =
    client
      .fareRules11(FareRules11Form.toRequest(form))
      .map(FareRules11Result.apply)

}

