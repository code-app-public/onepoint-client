package com.codeapp

import onepointentities.onepoint.{mystifly => om}
import org.datacontract.schemas._2004._07.{mystifly_onepoint, mystifly => m}

object ObjectFactory {

  val mystifly = new m.ObjectFactory()

  val mystiflyOnePoint = new mystifly_onepoint.ObjectFactory()

  val onePointEntityMystifly = new om.ObjectFactory()

}
