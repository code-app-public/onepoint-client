resolvers += "jitpack" at "https://jitpack.io"

name := "onepoint-client"

organization := "com.codeapp"

version := "0.3"

scalaVersion := "2.12.6"
scalacOptions ++= Seq("-deprecation", "-feature")

sources in (Compile,doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false

libraryDependencies ++= Seq(
  guice
  , ehcache
  , "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0" // Scala-based logging library wrapping SLF4J.
)

WsdlKeys.wsdlUrls in Compile += url("http://onepointdemo.myfarebox.com/V2/OnePoint.svc?singleWsdl")
WsdlKeys.wsdlToCodeArgs += "-autoNameResolution"
